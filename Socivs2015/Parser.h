#pragma once

#include<vector>
#include<string>
#include<regex>
#include"Command.h"
#include"Player.h"

class Parser
{
private:	
	std::vector<Command> m_AllCommands;
public:	
	bool parse(const std::string& toParse);
	void parseAndExecute(const std::string & toParse,Player& plr);
	void addComand(const Command& toAdd);
	void changeCommand(std::string cmdName, std::string shortcut);
	inline std::vector<Command> & allComandsRef() { return m_AllCommands; };
	std::string getAllComandsDescription();
};


