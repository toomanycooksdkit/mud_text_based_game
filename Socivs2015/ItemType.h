#pragma once
#include <type_traits> //for std::underlying_type
//c++11 the new enum class you can assign the walues as you wish for example troll can start at 8 or 20 whatever you want
enum class ItemType
{
	Weapon = 0 ,
	Armour,
	Potion,
	Key
};

template<typename E>
constexpr auto to_integral(E e) -> typename std::underlying_type<E>::type
{
	return static_cast<typename std::underlying_type<E>::type>(e);
}
