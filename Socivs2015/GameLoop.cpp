#include "GameLoop.h"
#include "AllOnlinePlayers.h"
#include "Telnet.h"


// declare the static instance of the timer
GameLoop::a_clock GameLoop::s_clock;
static long long s_startPoint;
static bool startTeacherTimerb;

constexpr GameLoop::sint64 DBSAVETIME = minToMS(15);
constexpr GameLoop::sint64 ROUNDTIME = secToMS(3);
constexpr GameLoop::sint64 REGENTIME = secToMS(30);
constexpr GameLoop::sint64 HEALTIME = secToMS(60);
constexpr GameLoop::sint64 TEACHERCHECK = secToMS(60);

void GameLoop::initialize()
{
	sint64 now = std::chrono::duration_cast<Ms>(a_clock::now().time_since_epoch()).count();
	s_startPoint = std::chrono::duration_cast<std::chrono::minutes>(a_clock::now().time_since_epoch()).count();//init the start
	startTeacherTimerb = false;
	m_nextheal = now;
	m_nextregen = now;
	m_nextround = now;
	m_savedatabases = now;
}

void GameLoop::LoadDatabases()
{
	Map::loadRooms();
	EnemyHandler::loadEnemies();
	ItemHandler::loadItems();
}

void GameLoop::SaveDatabases()
{
}

void GameLoop::Loop()
{
	sint64 now = std::chrono::duration_cast<Ms>(a_clock::now().time_since_epoch()).count();
	//std::cout << "Now: " << now << std::endl;
	if (now >= m_nextround)
	{
		//std::cout << "PERFORMING ENEMY ATT ROUND OF GAME: "<< std::endl;
		performEnemyAttack();		
		m_nextround += ROUNDTIME;
	}
	if (now >= m_nextregen)
	{
		//std::cout << "PERFORMING REGEN OF GAME: " << now << std::endl;
		m_nextregen += REGENTIME;
	}
	if (now >= m_nextheal)
	{
		//std::cout << "PERFORMING HEAL OF GAME: " << now << std::endl;
		stockUpStores();
		restoreMainGateKey();
		m_nextheal += HEALTIME;
	}

	if (now >= m_savedatabases)
	{
		//std::cout << "PERFORMING SAVE OF GAME: " << std::endl;
		performAutosave();
		m_savedatabases += DBSAVETIME;
	}

	if (startTeacherTimerb)
	{	
		if (!m_lock)
		{
			m_teacherCheck = now + TEACHERCHECK;
			m_lock = true;
		}	
		if (now >= m_teacherCheck)
		{
			//return teacher into principles office
			NPC teacher = Map::getRoomPtrFromId(6)->removeNPCbyID(2);
			Map::getRoomPtrFromId(14)->addNPC(teacher);	
			std::cout << "techer time ended" << std::endl;
			startTeacherTimerb = false;
			m_lock = false;
		}
	}

}

void GameLoop::performAutosave()
{
	OnlinePlayers::autoSaveAllPlayers();
}

//this is called every second or time specified by coder
void GameLoop::performEnemyAttack()
{
	Map::allEnemyAttacks();
}

void GameLoop::stockUpStores()
{
	std::cout << "Stocking up storest" << std::endl;
	Map::stockAllRooms();
}

void GameLoop::restoreMainGateKey()
{
	if (Map::getRoomPtrFromId(14)->m_items.empty())
	{
		Item itm;
		itm.setID(14);
		itm.setEntName("Main Gate Key");
		Map::getRoomPtrFromId(14)->addItem(itm);
		OnlinePlayers::sendAllPlayers(Lukas::green + "The Principle got a spare key cut");
	}
}

long long GameLoop::getStartPoint()
{
	return s_startPoint;
}

void GameLoop::startTeacherTimer()
{
	startTeacherTimerb = true;
}
