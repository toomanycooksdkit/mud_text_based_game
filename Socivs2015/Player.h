#pragma once

#include"Item.h"
#include"PlayerTypes.h"
#include"Level.h"
#include"Inventory.h"
#include"Room.h"
#include"ConnectionWithThread.h"
#include "Telnet.h"
#include "Connection.h"
#include <regex>
#include "HelperFunctions.h"
#include "Map.h"
#include "Party.h"

class Parser;

class Player
{
private:
	Entity m_entity;
	std::string m_password;
	BaseStats m_baseStats;
	PlayerRank m_rank{ PlayerRank::Regular };
	Level m_level;
	Inventory m_inventory;
	int m_money{ 0 };
	//Social type
	std::shared_ptr<Room> m_room_ShPtr;
	Room * m_room_ptr;
	bool pvpEnabled{ true };
	int m_roomID{ 0 };
	int m_speed{ 0 };
	int m_statPoints{ 0 };
	Item m_weapon;
	Item m_armor;
	//nonSaveable
	bool m_newbie{ true };
	SocketLib::Connection<Lukas::Telnet>* m_connection;

public:
	std::string m_lastData{"no_data"};
	bool m_isDistracting{ false };
	std::shared_ptr<Party> m_party;

	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };

	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };
	inline std::string getPassword() const { return m_password; };
	inline void setPassword(const std::string& toSet) { m_password = toSet; };


	inline int getSTR()const { return m_baseStats.m_STR; };
	inline void setSTR(int str) { m_baseStats.m_STR = str; };
	inline int getAGI()const { return m_baseStats.m_AGI; };
	inline void setAGI(int agi) { m_baseStats.m_AGI = agi; };
	inline std::string getBsName() const { return m_baseStats.m_BSTName; };
	inline void setBsName(const std::string& toSet) { m_baseStats.m_BSTName = toSet; };
	inline int getCurrHealth()const { return m_baseStats.m_Health.m_currentHealth; };
	inline void setCurrHealth(int toSet) { m_baseStats.m_Health.m_currentHealth = toSet; };
	inline int getRegenAmount()const { return m_baseStats.m_Health.m_regenAmount; };
	inline void setRegenAmount(int toSet) { m_baseStats.m_Health.m_regenAmount = toSet; };
	inline int getMaxHealth()const { return m_baseStats.m_Health.m_maxHealth; };
	inline void setMaxHealth(int toSet) { m_baseStats.m_Health.m_maxHealth = toSet; };
	inline PlayerRank getRank()const { return m_rank; };
	inline void setRank(PlayerRank toSet) { m_rank = toSet; };

	inline int getCurrExp()const { return m_level.m_experienceBar; };
	inline void setCurrExp(int toSet) { m_level.m_experienceBar = toSet; };
	inline int getCurrLevel()const { return m_level.m_curLevel; };
	inline void setCurrLevel(int toSet) { m_level.m_curLevel = toSet; };
	inline int getMaxLevel()const { return m_level.m_maxLevel; };
	inline void setMaxLevel(int toSet) { m_level.m_maxLevel = toSet; };
	inline int getNextLevel()const { return m_level.m_nextLevelXP; };
	inline void setNextLevel(int toSet) { m_level.m_nextLevelXP = toSet; };

	inline int getMoney()const { return m_money; };
	inline void setMoney(int toSet) { m_money = toSet; };

	inline bool getPvp()const { return pvpEnabled; };
	inline void setPvp(bool toSet) { pvpEnabled = toSet; };

	inline int getRoomID()const { return m_roomID; };
	inline void setRoomID(int toSet) { m_roomID = toSet; };

	inline int getSpeed()const { return m_speed; };
	inline void setSpeed(int toSet) { m_speed = toSet; };
	inline int getStatPoints()const { return m_statPoints; };
	inline void setStatPoints(int toSet) { m_statPoints = toSet; };

	inline Room& getRoomRef()const { return *m_room_ptr; };
	inline void setRoomPtr(Room * toSet) { m_room_ptr = toSet; };
	inline Inventory& getInventory() { return m_inventory; }

	inline SocketLib::Connection<Lukas::Telnet>*& getConn() { return m_connection; }
	inline void setConn(SocketLib::Connection<Lukas::Telnet>* toSet) { m_connection = toSet; };
	inline bool getNewbie()const { return m_newbie; };
	inline void setNewbie(bool toSet) { m_newbie = toSet; };
	std::string getRankStr();
	Parser & getParser();

	// ------------------------------------------------------------------------
	//  Aditional functions
	// ------------------------------------------------------------------------
	void subtractStatPoint();
	void addTobaseAttr(int p_attr, int val);
	void pickupItem();
	void pickupItemWithName();
	void dropItemWithName();
	void buyItem();
	void buyItemWithName();
	void sellItemWithName();
	void attackByName();
	bool canBuy(int price);
	void pickupMoney();
	void changeCommand();//it will change the data saved in player
	void findAndChangeCommand(std::string data);
	void repeatLastCommand();
	void kickAnotherPlayer();
	void banAnotherPlayer();
	void removeBanFromPlayer();
	void createParty();
	void sendParty();
	void joinParty();
	void leaveParty();
	void useItem(Item toUse); //it will check last command to figure out what he wanted to use
	void equipNewArmor(Item toUse);
	void equipNewWeapon(Item toUse);
	void usePotion(Item toUse);
	void removeEquippedArmorBonuses();
	void removeEquippedWeaponBonuses();
	void addEquippedArmorBonuses();
	void addEquippedWeaponBonuses();
	bool ifFoundUse();
	bool fightEnemy(Enemy &enemy);
	bool checkIfHitEnemy(Enemy &enemy);
	int calculateDamageOnEnemy(Enemy &enemy);
	void inflictDamageOnEnemy(Enemy &enemy, int damage);
	bool checkIfKillEnemy(Enemy &enemy);
	bool checkIfDead();
	void respawn();
	void leaveGame();
	void changeRankOfAnotherPlayer();
	void changeRankOfAnotherPlayerDown();
	void demotePlayer();
	void promotePlayer();
	void sendRoom(const std::string data);
	void distractIfPossible();
	void distractionStopped();
	void throwFireworks();
	void unlockGateAndTrain();
	void train();
	void aktOnSocialSkill();
	void whisperByName();

	//Parser printing functionality
	void printPlayer() const;
	void printExp();
	void printStats();
	void printCurrRoom();
	void printEnemies();
	void printNPCs();
	void printItems();
	void printMoney();
	void SendString(const std::string& toSend);
	void PrintStatbar(bool p_update = false);
	void printRoomFromPtr();
	void printInventory();
	void printEquipped();
	void printHelp();
	void printOnlinePlayers();
	void printAllPlayers();
	void printPlayersInRoom();

	////////////////////////////////////////////////
	void testFunc();
	bool fought(Player &plr);
	bool checkIfWasHit(Player &plr);
	int calcYourDmg(Player &plr);
	void getDamaged(Player &plr, int damage);
	bool checkIfKilled(Player &plr);
	void drawMap();
	void levelUpCheck();
	////////////////////////////////////////////////

	template<class Attackable>
	void testTemp(Attackable att);

	template<class Attackable>
	bool fight(Attackable &att);

	template<class Attackable>
	bool checkIfHit(Attackable &att);

	template<class Attackable>
	int calculateDamage(Attackable &att);

	template<class Attackable>
	void inflictDamage(Attackable &att, int damage);

	template<class Attackable>
	bool checkIfKill(Attackable &att);
	
};




namespace soci
{
	template<>
	struct type_conversion<Player>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Player& plr)
		{
			plr.setID(v.get<int>("entity_id"));
			plr.setEntName(v.get<std::string>("entity_name", "unknown"));
			plr.setPassword(v.get<std::string>("plr_password"));
			plr.setSTR(v.get<int>("str"));
			plr.setAGI(v.get<int>("agi"));
			plr.setBsName(v.get<std::string>("bs_name", "unknown"));
			plr.setCurrHealth(v.get<int>("curr_health"));
			plr.setRegenAmount(v.get<int>("regen_amount"));
			plr.setMaxHealth(v.get<int>("max_health"));
			plr.setRank(static_cast<PlayerRank>(v.get<int>("rank")));
			plr.setCurrExp(v.get<int>("curr_exp"));
			plr.setCurrLevel(v.get<int>("curr_level"));
			plr.setMaxLevel(v.get<int>("max_level"));
			plr.setNextLevel(v.get<int>("next_level_xp"));
			plr.setMoney(v.get<int>("money"));
			plr.setRoomID(v.get<int>("room_id"));
			plr.setSpeed(v.get<int>("att_speed"));
			plr.setStatPoints(v.get<int>("stat_points"));
		}


		static void to_base(const Player& plr, values& v, indicator& ind)
		{
			v.set("entity_id", plr.getID());
			v.set("entity_name", plr.getEntName(), plr.getEntName().empty() ? i_null : i_ok);
			v.set("plr_password", plr.getPassword());
			v.set("str", plr.getSTR());
			v.set("agi", plr.getAGI());
			v.set("bs_name", plr.getBsName());
			v.set("curr_health", plr.getCurrHealth());
			v.set("regen_amount", plr.getRegenAmount());
			v.set("max_health", plr.getMaxHealth());
			int p_rank = to_integral(plr.getRank());
			v.set("rank", p_rank);
			v.set("curr_exp", plr.getCurrExp());
			v.set("curr_level", plr.getCurrLevel());
			v.set("max_level", plr.getMaxLevel());
			v.set("next_level_xp", plr.getNextLevel());
			v.set("money", plr.getMoney());
			v.set("room_id", plr.getRoomID());
			v.set("att_speed", plr.getSpeed());
			v.set("stat_points", plr.getStatPoints());
			ind = i_ok;
		}

	};
}

template<class Attackable>
inline void Player::testTemp(Attackable att)
{
	att.testFunc();
}

template<class Attackable>
inline bool Player::fight(Attackable &att)
{
	return att.fought(*this);
}

template<class Attackable>
inline bool Player::checkIfHit(Attackable &att)
{
	return att.checkIfWasHit(*this);
}

template<class Attackable>
inline int Player::calculateDamage(Attackable &att)
{
	return att.calcYourDmg(this);
}

template<class Attackable>
inline void Player::inflictDamage(Attackable &att, int damage)
{
	att.getDamaged(*this, damage);
}

template<class Attackable>
inline bool Player::checkIfKill(Attackable &att)
{
	return att.checkIfKilled(*this);
}
