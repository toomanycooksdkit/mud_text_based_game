#include "EnemyType.h"

std::string typeToString(const EnemyType & type)
{	
	switch (type)
	{
	case EnemyType::Troll:
		return "Troll(s)";
	case EnemyType::Witch:
		return "Witch(es)";
	case EnemyType::Dragon:
		return "Dragon(os)";
	case EnemyType::Skeleton:
		return "Skeleton(os)";
	case EnemyType::Ghost:
		return "Ghost(s)";
	default:
		return "NoType";
	}
}
