#include "DateStampTimeStamp.h"

// ============================================================
// This prints a timestamp in 24 hours hh:mm:ss format
// ============================================================
std::string TimeStamp()
{
	char str[9];

	// get the time, and convert it to struct tm format
	time_t a = time(0);
	struct tm b;
	localtime_s(&b, &a);

	// print the time to the string
	strftime(str, 9, "%H:%M:%S", &b);

	return str;
}


// ============================================================
// This prints a datestamp in YYYY:MM:DD format
// ============================================================
std::string DateStamp()
{
	char str[11];

	// get the time, and convert it to struct tm format
	time_t a = time(0);
	struct tm b;
	localtime_s(&b, &a);

	// print the time to the string
	strftime(str, 11, "%Y.%m.%d", &b);

	return str;
}