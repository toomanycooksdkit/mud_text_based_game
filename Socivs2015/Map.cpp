#include "Map.h"
#include "ItemHandler.h"

std::vector<Room> m_allRooms;
//std::vector<std::shared_ptr<Room>> m_allRoomsSptrs;

using namespace soci;

void Map::loadRooms()
{
	std::cout << "Loading Rooms" << std::endl;

	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	int count;
	sql << "select count(*) from rooms", into(count);

	
	m_allRooms.reserve(count);
	Room room;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from rooms where entity_id = :entity_id", into(room), use(i);
		if (room.getID() == 14)//add NPC principle and teacher
		{
			room.addNPC(NPC("Principle", 1));
			Item itm;
			itm.setID(14);
			itm.setEntName("Main Gate Key");
			room.addItem(itm);

			room.addNPC(NPC("Teacher", 2));
		}
		m_allRooms.emplace_back(room);
	}

	std::cout << "Rooms Loaded" << std::endl;

}

void Map::saveRooms()
{
	std::cout << "Saving/updating Rooms" << std::endl;

	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	for (const auto& room : m_allRooms)
	{	
		//update room on save
		/*sql << "update rooms"
			" set bs_name = :bs_name"
			" where entity_name = :entity_name",
			use(room);*/
	}
	std::cout << "Rooms Saved need to finish the code" << std::endl;
}

void Map::printMap()
{
	for (const auto& room : m_allRooms)
	{
		room.printRoom();
	}
}

Room * Map::getRoomPtrFromId(int roomID)
{
	if (m_allRooms.size() - 1 >= roomID-1)
	{
		//std::cout << "returning room pointer" << std::endl;
		return &m_allRooms[roomID-1];
	}
	else
	{
		std::cout << "room not found in vector nullptr" << std::endl;
		return nullptr;
	}
}

void Map::allEnemyAttacks()
{
	for (auto & room : m_allRooms)
	{
		room.enemyAttacks();
	}
}

void Map::stockAllRooms()
{
	for (auto & room : m_allRooms)
	{
		if (room.m_roomType == RoomType::Shop)//if it is shop than put some items there
		{
			if (room.m_items.empty())
			{
				//put the special items into rooms
				Item fireworks;
				fireworks.setID(11);
				fireworks.setEntName("Fireworks");
				fireworks.m_baseStats.m_AGI = 0;
				fireworks.m_baseStats.m_STR = 0;
				fireworks.m_baseStats.m_BSTName = "Fireworks";
				fireworks.m_baseStats.m_Health.m_currentHealth = 0;
				fireworks.m_baseStats.m_Health.m_maxHealth = 0;
				fireworks.m_baseStats.m_Health.m_regenAmount = 0;
				fireworks.m_itemType = ItemType::Key;
				fireworks.m_price.sellable = true;
				fireworks.m_price.m_buyPrice = 100;
				fireworks.m_price.m_SellPrice = 70;

				room.getItemsOnFloorAfterKill(ItemHandler::generateRoomItemsAfterKill(5));
				room.addItem(fireworks);
			}
		}
	}
}



