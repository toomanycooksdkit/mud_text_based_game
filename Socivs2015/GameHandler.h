#pragma once

#include "Telnet.h"
//#include "ConnectionWithThread.h"

#include <string>
#include "PlayerTypes.h"

#include"Connection.h"

#include"Parser.h"
#include"CommandFunctions.h"
#include"Command.h"
#include <memory>
#include <functional>

namespace SimpleMUD
{
	using namespace Lukas;
	using std::string;

	class GameHandler : public Telnet::handler
	{
		typedef Telnet::handler thandler;
	public:

		GameHandler(Connection<Telnet>& p_conn, int p_pID)
			: thandler(p_conn)
		{
			std::cout << "Id of player in game handler: " << p_pID << std::endl;
			isLeft = false;
			m_pOnlineID = p_pID;

		}

		// ------------------------------------------------------------------------
		//  Handler Functions
		// ------------------------------------------------------------------------
		void Handle(string p_data);
		void Enter();
		void Leave();
		void Hungup();
		void Flooded();
		void GotoTrain();
		void populateParser();
		inline static bool& Running() { return s_running; };
		inline Parser& getParser() { return m_parser; };

	protected:
		Parser m_parser;
		int m_pOnlineID;
		static bool s_running;
		bool isLeft;

	};  // end class Game
}   // end namespace SimpleMUD

