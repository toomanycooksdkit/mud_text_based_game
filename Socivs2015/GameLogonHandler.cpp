#include "GameLogonHandler.h"
#include"GameHandler.h"
#include "soci.h"
#include "soci-postgresql.h"
#include"Map.h"
#include"Shortcuts.h"

using namespace Lukas;
constexpr int ALLOWED_CONN_FROM_ONE_IP{ 1 };

namespace SimpleMUD
{
	using namespace soci;
	// ------------------------------------------------------------------------
	//  This handles incomming commands. Anything passed into this function
	//  is assumed to be a complete command from a client.
	// ------------------------------------------------------------------------
	void Logon::Handle(string p_data)
	{		
		//this would be passed into the function instead of constructing it but no time
		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		std::string pass{ "empty" };
		std::string plr_name{ "empty" };

		if (m_errors == 5)
		{
			m_connection->Protocol().SendString(*m_connection, red + bold +
				"Too many incorrect responses, closing connection..." +
				newline);
			m_connection->Close();
			return;
		}


		if (m_state == NEWCONNECTION)  // has not entered name yet
		{
			if (BasicLib::LowerCase(p_data) == "new")
			{
				m_state = NEWUSER;
				m_connection->Protocol().SendString(*m_connection, yellow +
					"Please enter your desired name: " + reset);
			}
			else
			{
				sql << "select entity_name, plr_password from players where entity_name = :entity_name", into(plr_name), into(pass), use(p_data);				
				std::cout << pass << std::endl;
				std::cout << plr_name << std::endl;
				if (plr_name == "empty")
				{
					// name does not exist
					m_errors++;
					m_connection->Protocol().SendString(*m_connection,
						blue + bold + "Sorry, the user \"" + white + p_data + red +
						"\" does not exist.\r\n" +
						"Please enter your name, or \"new\" if you are new: " +
						reset);
				}
				else
				{
					// name exists, go to password entrance.
					m_state = ENTERPASS;
					m_name = p_data;
					m_pass = pass;

					m_connection->Protocol().SendString(*m_connection,
						green + bold + "Welcome, " + white + p_data + red +
						newline + green + "Please enter your password: " +
						reset);
				}
			}

			return;
		}

		if (m_state == NEWUSER)
		{
			// check if the name is taken:

			sql << "select entity_name, plr_password from players where entity_name = :entity_name", into(plr_name), into(pass), use(p_data);

			if (plr_name != "empty")
			{
				m_errors++;
				m_connection->Protocol().SendString(*m_connection,
					red + bold + "Sorry, the name \"" + white + p_data + red +
					"\" has already been taken." + newline + yellow +
					"Please enter your desired name: " + reset);
			}
			else
			{
				if (!AcceptibleName(p_data))
				{
					m_errors++;
					m_connection->Protocol().SendString(*m_connection,
						red + bold + "Sorry, the name \"" + white + p_data + red +
						"\" is unacceptible." + newline + yellow +
						"Please enter your desired name: " + reset);
				}
				else
				{
					m_state = ENTERNEWPASS;
					m_name = p_data;
					m_connection->Protocol().SendString(*m_connection,
						green + "Please enter your desired password: " +
						reset);
				}
			}

			return;
		}

		if (m_state == ENTERNEWPASS)
		{
			if (p_data.find_first_of(BasicLib::WHITESPACE) != string::npos)
			{
				m_errors++;
				m_connection->Protocol().SendString(*m_connection,
					red + bold + "INVALID PASSWORD!" +
					green + "Please enter your desired password: " +
					reset);
				return;
			}

			m_connection->Protocol().SendString(*m_connection,
				green + "Thank you! You are now entering the realm and are inserted in the database..." +
				newline);
			//insert new player into database
			Player p;		
			p.setEntName(m_name);
			p.setPassword(p_data);
			p.setRank(PlayerRank::Regular);
			p.setAGI(10);
			p.setSTR(10);
			p.setStatPoints(10);
			p.setCurrHealth(50);
			p.setMaxHealth(50);
			p.setRegenAmount(10);
			p.setCurrExp(0);
			p.setCurrLevel(1);
			p.setMaxLevel(100);
			p.setNextLevel(50);
			p.setMoney(10);
			p.setRoomID(1);
			p.setSpeed(5);
			int count;
			sql << "select count(*) from players", into(count);
			p.setID(count);
			// add the player into the database

			sql << "insert into players(entity_id, entity_name, plr_password, str, agi, bs_name, curr_health, regen_amount, max_health, rank, curr_exp, curr_level,  max_level, next_level_xp, money, room_id, att_speed, stat_points) "
				"values(:entity_id, :entity_name, :plr_password, :str, :agi, :bs_name, :curr_health, :regen_amount, :max_health, :rank, :curr_exp, :curr_level, :max_level, :next_level_xp, :money, :room_id, :att_speed, :stat_points)", use(p);
			
			//populate his shortcuts with common shortcuts
			Shortcuts plrShortcuts;
			plrShortcuts.setEntName(m_name);
			plrShortcuts.setID(count);
			plrShortcuts.m_shortcuts.emplace_back("chang");
			plrShortcuts.m_shortcuts.emplace_back("x");
			plrShortcuts.m_shortcuts.emplace_back("ch");
			plrShortcuts.m_shortcuts.emplace_back("sh");
			plrShortcuts.m_shortcuts.emplace_back("n");
			plrShortcuts.m_shortcuts.emplace_back("s");
			plrShortcuts.m_shortcuts.emplace_back("e");
			plrShortcuts.m_shortcuts.emplace_back("w");
			plrShortcuts.m_shortcuts.emplace_back("wh");
			plrShortcuts.m_shortcuts.emplace_back("a");
			plrShortcuts.m_shortcuts.emplace_back("p");
			plrShortcuts.m_shortcuts.emplace_back("u");
			plrShortcuts.m_shortcuts.emplace_back("h");
			plrShortcuts.m_shortcuts.emplace_back("who");
			plrShortcuts.m_shortcuts.emplace_back("whll");
			plrShortcuts.m_shortcuts.emplace_back("prm");
			plrShortcuts.m_shortcuts.emplace_back("de");
			plrShortcuts.m_shortcuts.emplace_back("l");
			plrShortcuts.m_shortcuts.emplace_back("g");
			plrShortcuts.m_shortcuts.emplace_back("dd");

			sql << "insert into shortcuts(entity_id, entity_name, change, stat, chat, shut, north, south, east, west, whisper, attack, pick, use, help, who, whoall, promote, demote, look, get, distract ) "
			"values(:entity_id, :entity_name, :change, :stat, :chat, :shut, :north, :south, :east, :west, :whisper, :attack, :pick, :use, :help, :who, :whoall, :promote, :demote, :look, :get, :distract ) ", use(plrShortcuts);

			sql << "insert into inventory(entity_id, entity_name, pos0, pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9 ) "
				"values(:entity_id, :entity_name, :pos0, :pos1, :pos2, :pos3, :pos4, :pos5, :pos6, :pos7, :pos8, :pos9 )", use(p.getID(), "entity_id"), use(p.getEntName(), "entity_name"), use(p.getInventory());

			// enter the game as a newbie.
			GotoGame(true);
			return;
		}

		if (m_state == ENTERPASS)
		{
			if (m_pass == p_data)
			{
				m_connection->Protocol().SendString(*m_connection,
					green + "Thank you! You are now entering the realm..." +
					newline);
				// enter the game
				GotoGame(false);
			}
			else
			{
				m_errors++;
				m_connection->Protocol().SendString(*m_connection,
					red + bold + "INVALID PASSWORD!" + newline +
					yellow + "Please enter your password: " +
					reset);
			}

			return;
		}
	}


	// ------------------------------------------------------------------------
	//  This notifies the handler that there is a new connection
	// ------------------------------------------------------------------------
	void Logon::Enter()
	{
		USERLOG.Log(
			GetIPString(m_connection->GetRemoteAddress()) +
			" - entered login state.");
		//check all online players for theyr IP adresses so only one IP adress can be connected at the time

		/*if (OnlinePlayers::isConnected(m_connection->GetRemoteAddress()))
		{
			USERLOG.Log(
				GetIPString(m_connection->GetRemoteAddress()) +
				" - IP already connected going to disconnect.");
			m_connection->Close();
		}*/
		if (OnlinePlayers::isConnectedNumberOfTimes(m_connection->GetRemoteAddress(), ALLOWED_CONN_FROM_ONE_IP))
		{
			USERLOG.Log(
				GetIPString(m_connection->GetRemoteAddress()) +
				" - IP already connected more than " + BasicLib::tostring(ALLOWED_CONN_FROM_ONE_IP) +" times going to disconnect");
			m_connection->Close();
		}
		else
		{
			m_connection->Protocol().SendString(*m_connection,"\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #     #                                              #######\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #  #  # ###### #       ####   ####  #    # ######       #     ####\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #  #  # #      #      #    # #    # ##  ## #            #    #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #  #  # #####  #      #      #    # # ## # #####        #    #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #  #  # #      #      #      #    # #    # #            #    #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"      #  #  # #      #      #    # #    # #    # #            #    #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"       ## ##  ###### ######  ####   ####  #    # ######       #     ####\r\n");
			m_connection->Protocol().SendString(*m_connection,"\r\n");
			m_connection->Protocol().SendString(*m_connection,"  #     #                                             #     #\r\n");
			m_connection->Protocol().SendString(*m_connection,"  ##   ##  ####  #    #  ####  ##### ###### #####     #     # #  ####  #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"  # # # # #    # ##   # #        #   #      #    #    #     # # #    # #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"  #  #  # #    # # #  #  ####    #   #####  #    #    ####### # #      ######\r\n");
			m_connection->Protocol().SendString(*m_connection,"  #     # #    # #  # #      #   #   #      #####     #     # # #  ### #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"  #     # #    # #   ## #    #   #   #      #   #     #     # # #    # #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"  #     #  ####  #    #  ####    #   ###### #    #    #     # #  ####  #    #\r\n");
			m_connection->Protocol().SendString(*m_connection,"\r\n");
			m_connection->Protocol().SendString(*m_connection,"                   #####\r\n");
			m_connection->Protocol().SendString(*m_connection,"                  #     #  ####  #    #  ####   ####  #\r\n");
			m_connection->Protocol().SendString(*m_connection,"                  #       #    # #    # #    # #    # #\r\n");
			m_connection->Protocol().SendString(*m_connection,"                   #####  #      ###### #    # #    # #\r\n");
			m_connection->Protocol().SendString(*m_connection,"                        # #      #    # #    # #    # #\r\n");
			m_connection->Protocol().SendString(*m_connection,"                  #     # #    # #    # #    # #    # #\r\n");
			m_connection->Protocol().SendString(*m_connection,"                   #####   ####  #    #  ####   ####  ######\r\n");
			m_connection->Protocol().SendString(*m_connection,"\r\n");
			m_connection->Protocol().SendString(*m_connection, "Please enter your name, or \"new\" if you are new: ");

		}
	}

	// ------------------------------------------------------------------------
	//  This changes the game state so that the player enters the game.
	// ------------------------------------------------------------------------
	void Logon::GotoGame(bool p_newbie)
	{
		Player p;
		std::cout << "LOADING PLAYER" << std::endl;
		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		int bannedDays{ 0 };
		sql << "select days from bannedplayers where name = :name", into(bannedDays), use(m_name);
		if (bannedDays != 0)
		{
			m_connection->Protocol().SendString(*m_connection,
				red + bold + "You have been banned for your recent behaviour for: " + BasicLib::tostring(bannedDays) +
				"day/s login after the ban expires.");
			m_connection->Close();
		}
		else
		{
			sql << "select * from players where entity_name = :entity_name", into(p), use(m_name);
			sql << "select * from inventory where entity_id = :entity_id", into(p.getInventory()), use(p.getID());
			//after the inventory is populated get the items from ids

			p.getInventory().getItemsFromIds();
			Player& p_fromOnlineRef2 = OnlinePlayers::addPlayerWithReturnRef(p);
			p_fromOnlineRef2.setRoomPtr(Map::getRoomPtrFromId(p_fromOnlineRef2.getRoomID()));
			p_fromOnlineRef2.getRoomRef().storePlayer(p_fromOnlineRef2.getID());
			std::cout << "Player& p_fromOnlineRef2 ID : " << &p_fromOnlineRef2 << std::endl;
			p_fromOnlineRef2.setNewbie(p_newbie);
			p_fromOnlineRef2.setConn(m_connection);

			// go to the game.

			p_fromOnlineRef2.getConn()->RemoveHandler();
			p_fromOnlineRef2.getConn()->AddHandler(new GameHandler(*p_fromOnlineRef2.getConn(), p_fromOnlineRef2.getID()));
		}
	}

	// ------------------------------------------------------------------------
	//  This checks if a user name is acceptible.
	// ------------------------------------------------------------------------
	bool Logon::AcceptibleName(const string& p_name)
	{
		static string inv = " \"'~!@#$%^&*+/\\[]{}<>()=.,?;:";

		// must not contain any invalid characters
		if (p_name.find_first_of(inv) != string::npos)
			return false;

		// must be less than 17 chars and more than 2
		if (p_name.size() > 16 || p_name.size() < 3)
			return false;

		// must start with an alphabetical character
		if (!isalpha(p_name[0]))
			return false;

		if (p_name == "new")
			return false;

		return true;
	}



}   // end namespace SimpleMUD

