#pragma once
#include <array>
#include <string>

struct Party
{
	std::array<int, 5> m_members = {-1,-1,-1,-1,-1};
	std::string m_password{ "0000" };

	bool addMember(int plrID, const std::string & pass);
	bool removeMember(int plrID);
	void sendParty(const std::string & msg);

};