#pragma once

#include <random>


int getRandomIntWithSeed(int seed, int min, int max);

int getRandomMt19937(int min, int max);

int getRandomCryptoSecure(int min, int max);

constexpr inline long long minToMS(long long t) { return t * 60 * 1000; };
constexpr inline long long secToMS(long long t) { return t * 1000; };