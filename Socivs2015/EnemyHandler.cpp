#include "EnemyHandler.h"

std::vector<Enemy> m_PossibleEnemies;

using namespace soci;

void EnemyHandler::loadEnemies()
{
	std::cout << "Loading Enemis" << std::endl;

	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	int count;
	sql << "select count(*) from enemies", into(count);


	m_PossibleEnemies.reserve(count);
	Enemy enemy;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from enemies where entity_id = :entity_id", into(enemy), use(i);
		m_PossibleEnemies.emplace_back(enemy);
	}

	std::cout << "Enemis Loaded" << std::endl;

}

void EnemyHandler::saveEnemies()
{
	std::cout << "Saving/updating enemies" << std::endl;

	session sql(postgresql, "dbname=GameDB user=postgres password=password");

	for (const auto& enemy : m_PossibleEnemies)
	{

	}
	std::cout << "enemies Saved need to finish the code" << std::endl;
}

void EnemyHandler::add(Enemy & toAdd)
{
	m_PossibleEnemies.push_back(toAdd);
}

std::vector<Enemy> EnemyHandler::generateRoomEnemys(int number)
{
	std::vector<Enemy> toRetEnem;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector
	for (int i = 0;i < number;i++)
	{
		generatedNum = getRandomMt19937(0, m_PossibleEnemies.size()-1);
		toRetEnem.push_back(m_PossibleEnemies[generatedNum]);
	}

	return toRetEnem;
}

std::list<Enemy> EnemyHandler::generateRoomEnemysList(int number)
{
	std::list<Enemy> toRetEnem;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector
	for (int i = 0; i < number; i++)
	{
		generatedNum = getRandomMt19937(0, m_PossibleEnemies.size() - 1);
		toRetEnem.push_back(m_PossibleEnemies[generatedNum]);
	}

	return toRetEnem;
}

std::vector<Enemy> EnemyHandler::generateRoomEnemysSeedet(int number, int seed)
{
	std::vector<Enemy> toRetEnem;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector

	for (int i = 0;i < number;i++)
	{
		generatedNum = getRandomIntWithSeed(seed + i,0, m_PossibleEnemies.size() - 1);
		toRetEnem.push_back(m_PossibleEnemies[generatedNum]);
	}

	return toRetEnem;
}

void EnemyHandler::shuffleVector()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::shuffle(m_PossibleEnemies.begin(), m_PossibleEnemies.end(), mt);
}

void EnemyHandler::printEnemies()
{
	for (const auto& enemy : m_PossibleEnemies)
	{
		enemy.printEnemy();
	}
}
