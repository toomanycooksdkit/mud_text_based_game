#include "SocketLibSystemWord.h"

namespace Lukas
{
	SystemWord::SystemWord()
	{
		int iResult = WSAStartup(MAKEWORD(2, 2), &m_WSAData);
		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
			WSACleanup();
			system("PAUSE");
		}
	}

	SystemWord::~SystemWord()
	{
		WSACleanup();
	}

	// ========================================================================
	// Function:    GetIPString
	// Purpose:     Converts an ipaddress structure to a string in numerical
	//              format.
	// ========================================================================
	std::string GetIPString(ipaddress p_address)
	{
		// return a new string containing the address.
		// (god that is some ugly casting going on... stupid language)
		struct sockaddr_in sa;
		sa.sin_addr.s_addr = p_address;
		char str[INET_ADDRSTRLEN];
		inet_ntop(AF_INET, &sa.sin_addr, str, INET_ADDRSTRLEN);
		if (str == 0)
		{
			return std::string("Invalid IP Address");
		}
		return std::string(str);
	}
}