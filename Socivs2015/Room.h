#pragma once
#include <list>

#include"Item.h"
#include"RoomExits.h"
#include"RoomType.h"
#include"Enemy.h"
#include"EnemyHandler.h"
#include<list>
#include"NPC.h"

class AllOnlinePlayers;

class Room
{
private:
	Entity m_entity;
public:
	RoomExits m_exits;
	std::string m_description{ "noRoomDescription" };
	RoomType m_roomType{ RoomType::Standard };
	std::list<Enemy> m_enemies;
	std::list<Item> m_items;
	std::list<int> m_playersInRoom;
	std::vector<NPC> m_NPCsInRoom;
	int m_money{0}; //not saved yet


	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

	//aditional functions
	void printRoom() const;
	std::string getPlayerNamesInRoom();
	void storePlayer(int p);//Player &plr);
	void removePlayer(int p);
	void sendAllInRoom(const std::string & data);
	void addNPC(NPC toAdd);
	void addItem(Item toAdd);
	NPC * getNPCptr(int id);
	NPC removeNPCbyID(int id);
	bool removeEnemyByType(const EnemyType & type);
	void ifEmptyGenerateEnemies();
	void getItemsOnFloorAfterKill(std::list<Item> itemsAfterKill);
	void getMoneyOnFloorAfterKill(int money);
	Item removeFirstPickedItem();
	Item removeFirstBoughtItem(Player & plr);
	Item removeItemByName(std::string toRemove);
	Item removeBoughtItemByName(std::string toRemove, Player & plr);
	void attackPlayerByName(std::string attPlr, Player plr);
	void whisperToPlayerByName(std::string whisperPlr, std::string chat, Player plr);
	void enemyAttacks();
};


namespace soci
{
	template<>
	struct type_conversion<Room>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Room& rm)
		{
			rm.setID(v.get<int>("entity_id"));
			rm.setEntName(v.get<std::string>("entity_name", "unknown"));
			rm.m_exits.m_east = v.get<int>("east");
			rm.m_exits.m_north = v.get<int>("north");
			rm.m_exits.m_south = v.get<int>("south");
			rm.m_exits.m_west = v.get<int>("west");
			rm.m_description = v.get<std::string>("description");
			rm.m_roomType = static_cast<RoomType>(v.get<int>("room_type"));	
		}


		static void to_base(const Room& rm, values& v, indicator& ind)
		{
			v.set("entity_id", rm.getID());
			v.set("entity_name", rm.getEntName(), rm.getEntName().empty() ? i_null : i_ok);
			v.set("east", rm.m_exits.m_east);
			v.set("north", rm.m_exits.m_north);
			v.set("south", rm.m_exits.m_south);
			v.set("west", rm.m_exits.m_west);
			v.set("description", rm.m_description);
			int roomType = to_integral(rm.m_roomType);
			v.set("room_type", roomType);
			ind = i_ok;
		}

	};
}











