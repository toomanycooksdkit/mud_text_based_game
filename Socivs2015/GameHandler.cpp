#include "GameHandler.h"
#include "BasicLibString.h"
#include"SimpleMUDLogs.h"
#include"AllOnlinePlayers.h"
#include "Map.h"
#include "soci.h"
#include "soci-postgresql.h"
#include "TrainHandler.h"
#include "Shortcuts.h"

using namespace soci;

using namespace Lukas;
using namespace BasicLib;
using std::string;

namespace SimpleMUD
{
	bool GameHandler::s_running = true;

	void GameHandler::Handle(string p_data)
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		if (p_data == "rp" || BasicLib::LowerCase(p_data) == "repeat")//if is not the repeat command than record it
		{
			//dont record last data
		}
		else
		{
			p.m_lastData = p_data;
		}

		m_parser.parseAndExecute(p_data, p);

	}


	/* ------------------------------------------------------------------------
	  This notifies the handler that there is a new connection
	 ------------------------------------------------------------------------*/
	void GameHandler::Enter()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		USERLOG.Log(
			GetIPString(m_connection->GetRemoteAddress()) +
			" - User " + p.getEntName() +
			" entering Game state.");

		OnlinePlayers::sendAllPlayers(bold + green + p.getEntName()+ " has entered the realm :)");
		std::cout << "populating players commands" << std::endl;
		populateParser();

		if (p.getNewbie())
			GotoTrain();
		else
			p.printCurrRoom();

	}

	void GameHandler::Leave()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		USERLOG.Log(
			GetIPString(m_connection->GetRemoteAddress()) +
			" - User " + p.getEntName() +
			" leaving Game state.");
		// remove the player from his room
		p.getRoomRef().removePlayer(p.getID());

		std::cout << "Leaving game handler" << std::endl;
		std::cout << "Player id is " << m_pOnlineID << std::endl;

		if (m_connection->Closed())
		{
			OnlinePlayers::logoutPlayer(m_pOnlineID, m_parser);		
		}
		isLeft = true;
	}


	//------------------------------------------------------------------------
	// This notifies the handler that a connection has unexpectedly hung up.
	//------------------------------------------------------------------------
	void GameHandler::Hungup()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		USERLOG.Log(
			GetIPString(m_connection->GetRemoteAddress()) +
			" - User " + p.getEntName() +
			" hung up.");
	}

	/*------------------------------------------------------------------------
	 This notifies the handler that a connection is being kicked due to
	 flooding the server.
	------------------------------------------------------------------------*/
	void GameHandler::Flooded()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		USERLOG.Log(
			GetIPString(m_connection->GetRemoteAddress()) +
			" - User " + p.getEntName() +
			" flooded.");
	}

	void GameHandler::GotoTrain()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_pOnlineID);
		OnlinePlayers::sendAllPlayers(p.getEntName() + " leaves to edit stats");
		p.getConn()->AddHandler(new Train(*p.getConn(), p.getID()));
	}

	void GameHandler::populateParser()
	{
		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		Shortcuts plrShortcuts;
		sql << "select * from shortcuts where entity_id = :entity_id", into(plrShortcuts), use(m_pOnlineID, "entity_id");

		using namespace std::placeholders;  // for _1, _2, _3...
		std::function <void(Player&)> f = std::bind(changeCommand, _1);
		Command myCommand("change", plrShortcuts.m_shortcuts[0], f);
		myCommand.setDescription("Change <command> to <shortcut>");
		m_parser.addComand(myCommand);

		f = std::bind(printPlayerStats, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[1]);
		myCommand.setDescription("Shows your statistic");
		myCommand.setCommandStr("stat");

		m_parser.addComand(myCommand);

		//another command
		f = std::bind(chatToAll, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[2]);
		myCommand.setDescription("Send message to all players");
		myCommand.setCommandStr("chat");
		m_parser.addComand(myCommand);

		//another command
		f = std::bind(shutdownServer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[3]);
		myCommand.setDescription("Shuts down the server [Admin only]");
		myCommand.setCommandStr("shut");
		m_parser.addComand(myCommand);

		f = std::bind(goNorth, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[4]);
		myCommand.setDescription("Moves you north");
		myCommand.setCommandStr("north");
		m_parser.addComand(myCommand);

		f = std::bind(goSouth, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[5]);
		myCommand.setDescription("Moves you south");
		myCommand.setCommandStr("south");
		m_parser.addComand(myCommand);

		f = std::bind(goEast, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[6]);
		myCommand.setDescription("Moves you east");
		myCommand.setCommandStr("east");
		m_parser.addComand(myCommand);

		f = std::bind(goWest, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[7]);
		myCommand.setDescription("Moves you west");
		myCommand.setCommandStr("west");
		m_parser.addComand(myCommand);

		f = std::bind(whisper, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[8]);
		myCommand.setDescription("Whispers <player> private message");
		myCommand.setCommandStr("whisper");
		m_parser.addComand(myCommand);

		f = std::bind(attack, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[9]);
		myCommand.setDescription("Attack <player> or <enemy> if it is possible");
		myCommand.setCommandStr("attack");
		m_parser.addComand(myCommand);

		f = std::bind(pickItem, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[10]);
		myCommand.setDescription("Pick <item> from the floor");
		myCommand.setCommandStr("pick");
		m_parser.addComand(myCommand);

		f = std::bind(useItem, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[11]);
		myCommand.setDescription("Use <item> from inventory");
		myCommand.setCommandStr("use");
		m_parser.addComand(myCommand);

		f = std::bind(help, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[12]);
		myCommand.setDescription("Shows all available commands");
		myCommand.setCommandStr("help");
		m_parser.addComand(myCommand);

		f = std::bind(whoIsOnline, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[13]);
		myCommand.setDescription("Shows online players");
		myCommand.setCommandStr("who");
		m_parser.addComand(myCommand);

		f = std::bind(whoIsInDatabase, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[14]);
		myCommand.setDescription("Shows online and offline players");
		myCommand.setCommandStr("whoall");
		m_parser.addComand(myCommand);

		f = std::bind(promotePlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[15]);
		myCommand.setDescription("Promote <players> rank [Admin/God only]");
		myCommand.setCommandStr("promote");
		m_parser.addComand(myCommand);

		f = std::bind(demotePlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[16]);
		myCommand.setDescription("Demote <players> rank [Admin/God only]");
		myCommand.setCommandStr("demote");
		m_parser.addComand(myCommand);

		f = std::bind(lookAtRoom, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[17]);
		myCommand.setDescription("Look at room");
		myCommand.setCommandStr("look");
		m_parser.addComand(myCommand);

		f = std::bind(getMoney, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[18]);
		myCommand.setDescription("Get money from room");
		myCommand.setCommandStr("get");
		m_parser.addComand(myCommand);

		f = std::bind(distract, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut(plrShortcuts.m_shortcuts[19]);
		myCommand.setDescription("Distracts schools principal in his office");
		myCommand.setCommandStr("distract");
		m_parser.addComand(myCommand);

		f = std::bind(getTime, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("t");
		myCommand.setCommandStr("time");
		myCommand.setDescription("Shows system time");
		m_parser.addComand(myCommand);

		f = std::bind(buyItem, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("b");
		myCommand.setCommandStr("buy");
		myCommand.setDescription("Buys <item> from the store");
		m_parser.addComand(myCommand);

		f = std::bind(sellItem, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("se");
		myCommand.setCommandStr("sell");
		myCommand.setDescription("Sells <item> to the store");
		m_parser.addComand(myCommand);

		f = std::bind(throwFirewrks, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("th");
		myCommand.setCommandStr("throw");
		myCommand.setDescription("Throws <fireworks> to the boys toilet");
		m_parser.addComand(myCommand);

		f = std::bind(unlockGate, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("un");
		myCommand.setCommandStr("unlock");
		myCommand.setDescription("Unlocks main gate with Main Gate Key only");
		m_parser.addComand(myCommand);

		f = std::bind(steal, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("st");
		myCommand.setCommandStr("steal");
		myCommand.setDescription("Steals Main Gate Key from principles office");
		m_parser.addComand(myCommand);

		f = std::bind(optInOutPVP, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("pvp");
		myCommand.setCommandStr("OptInOutPvp");
		myCommand.setDescription("Changes your PvP mode. (Fighting > Passive / Passive > Fighting)");
		m_parser.addComand(myCommand);

		f = std::bind(dropItem, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("dr");
		myCommand.setCommandStr("drop");
		myCommand.setDescription("Drops <item> on the floor");
		m_parser.addComand(myCommand);

		f = std::bind(sayToRoom, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("sr");
		myCommand.setCommandStr("say");
		myCommand.setDescription("Say message in the current room");
		m_parser.addComand(myCommand);

		f = std::bind(plr_leaveGame, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("lv");
		myCommand.setCommandStr("leave");
		myCommand.setDescription("Leave the game (logout)");
		m_parser.addComand(myCommand);

		f = std::bind(repeatLastCommand, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("rp");
		myCommand.setCommandStr("repeat");
		myCommand.setDescription("Repeats last command");
		m_parser.addComand(myCommand);

		f = std::bind(kickPlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("kck");
		myCommand.setCommandStr("kick");
		myCommand.setDescription("Kicks <name> from server [Admin/God only]");
		m_parser.addComand(myCommand);

		f = std::bind(banPlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("bb");
		myCommand.setCommandStr("ban");
		myCommand.setDescription("Bans <name> from server [Admin only]");
		m_parser.addComand(myCommand);

		f = std::bind(removeBanFromPlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("ub");
		myCommand.setCommandStr("unban");
		myCommand.setDescription("Remove ban from <name> player [Admin only]");
		m_parser.addComand(myCommand);

		f = std::bind(createParty, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("crt");
		myCommand.setCommandStr("create");
		myCommand.setDescription("Create <password> party with password or default passwor is 0000");
		m_parser.addComand(myCommand);

		f = std::bind(sendParty, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("ptr");
		myCommand.setCommandStr("party");
		myCommand.setDescription("Sends message to the party");
		m_parser.addComand(myCommand);

		f = std::bind(joinParty, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("j");
		myCommand.setCommandStr("join");
		myCommand.setDescription("Join <name> <password> party.");
		m_parser.addComand(myCommand);

		f = std::bind(leaveParty, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("rs");
		myCommand.setCommandStr("resign");
		myCommand.setDescription("Leave the party.");
		m_parser.addComand(myCommand);

		f = std::bind(trainPlayer, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("trn");
		myCommand.setCommandStr("train");
		myCommand.setDescription("Train your character.");
		m_parser.addComand(myCommand);

		f = std::bind(aktOnSocialSkill, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("ak");
		myCommand.setCommandStr("act");
		myCommand.setDescription("Akt/talk to the monsters with same social skill.");
		m_parser.addComand(myCommand);

		f = std::bind(drawMap, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("map");
		myCommand.setCommandStr("drawmap");
		myCommand.setDescription("Print out the map");
		m_parser.addComand(myCommand);

		f = std::bind(levelUp, _1);
		myCommand.setFunc(f);
		myCommand.setShortcut("lvlUp");
		myCommand.setCommandStr("LevelUp");
		myCommand.setDescription("Check to Level Up");
		m_parser.addComand(myCommand);
	}
}