#include "Player.h"
#include"SimpleMUDLogs.h"
#include"BasicLibString.h"
#include"AllOnlinePlayers.h"
#include"GameHandler.h"
#include"GameLoop.h"
#include "Parser.h"
#include "soci.h"
#include "soci-postgresql.h"
#include "EnemyType.h"

using namespace Lukas;
using namespace BasicLib;
using namespace soci;

void Player::printPlayer() const
{
	m_entity.printEntity();
	std::cout << "Password: " << m_password << std::endl;
}

void Player::printExp()
{
	SendString("Your experience is : " + tostring(getCurrExp()));
}

void Player::printStats()
{
	std::string s = yellow +
		"---------------------------------- Your Stats ----------------------------------\r\n" +
		" Name:          " + getEntName() + "\r\n" +
		" Rank:          " + getRankStr() + "\r\n" +
		" HP/Max:        " + tostring(this->getCurrHealth()) + "/" + tostring(this->getMaxHealth()) + "\r\n" +
		" Strength:      " + tostring(this->getSTR()) + "\r\n" +
		" Health:        " + tostring(this->getCurrHealth()) + "\r\n" +
		" Agility:       " + tostring(this->getAGI()) + "\r\n" +
		" Money:		$" + tostring(this->getMoney()) + "\r\n" +
		" Level:		" + tostring(this->getCurrLevel()) + "\r\n" +
		" Exp:		" + tostring(this->getNextLevel()) + "\r\n";
		//"--------------------------------------------------------------------------------";
	SendString(s);
	printEquipped();
	printInventory();
}

void Player::printCurrRoom()
{
	std::string s = m_room_ptr->getEntName() + "\r\n" 
		+  m_room_ptr->m_description + "\r\n" + m_room_ptr->m_exits.availableExits();

	SendString(s);
	printPlayersInRoom();
	printNPCs();
	printEnemies();//sending automatic
	printItems();
	printMoney();
}

void Player::printInventory()
{
	std::string temp{ yellow + "---------------------------------- Inventory ----------------------------------\r\n" };
	for (auto& item : m_inventory.m_items)
	{
		if (item.getID() != 0)
		{
			temp += item.getEntName() + "			buy price $" + tostring(item.m_price.m_buyPrice)  + " sell price $" + tostring(item.m_price.m_SellPrice) + "\r\n";
		}
	}
	SendString(temp);
}

void Player::printEquipped()
{
	std::string temp{ yellow + "---------------------------------- Equipped ----------------------------------\r\n" };
	if(m_weapon.getID()!=0)
	temp += "Weapon:		 " + m_weapon.getEntName() + "\r\n";
	if(m_armor.getID()!=0)
	temp += "Armor:			 " + m_armor.getEntName() + "\r\n";

	SendString(temp);
}

void Player::printHelp()
{
	SendString(static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->getParser().getAllComandsDescription());
}

void Player::printOnlinePlayers()
{
	SendString(OnlinePlayers::getAllOnlineNames());
}

void Player::printAllPlayers()
{

}

void Player::printPlayersInRoom()
{
	SendString(m_room_ptr->getPlayerNamesInRoom());
}

void Player::changeRankOfAnotherPlayer()
{
	for (auto & player : OnlinePlayers::getOnlinePlayersRef())
	{
		std::string REGEX_GET = R"(\b()" + player.getEntName() + R"()\b)";

		std::cout << REGEX_GET << std::endl;
		std::cout << m_lastData << std::endl;

		std::regex rx(REGEX_GET);
		std::smatch pieces_match;
		if (std::regex_search(m_lastData, pieces_match, rx))
		{
			std::cout << "PLAYER found going to promote it: " << player.getEntName() << std::endl;
			if (m_rank == PlayerRank::Admin)
			{
				player.promotePlayer();
			}
		}
	}
}

void Player::changeRankOfAnotherPlayerDown()
{
	for (auto & player : OnlinePlayers::getOnlinePlayersRef())
	{
		std::string REGEX_GET = R"(\b()" + player.getEntName() + R"()\b)";

		std::cout << REGEX_GET << std::endl;
		std::cout << m_lastData << std::endl;

		std::regex rx(REGEX_GET);
		std::smatch pieces_match;
		if (std::regex_search(m_lastData, pieces_match, rx))
		{
			std::cout << "PLAYER found going to promote it: " << player.getEntName() << std::endl;
			if (m_rank == PlayerRank::Admin)
			{
				player.demotePlayer();
			}
		}
	}
}

void Player::demotePlayer()
{
	switch (m_rank)
	{
	case PlayerRank::Regular:
		SendString("Someone wands to demote you but cant");
		return;
	case PlayerRank::God:
		SendString("You have been demoted");
		m_rank = PlayerRank::Regular;
		return;
	case PlayerRank::Admin:
		SendString("You have been demoted");
		m_rank = PlayerRank::God;
		return;
	default:
		SendString("ERROR in demoted");
		return;
	}
}


void Player::promotePlayer()
{
	switch (m_rank)
	{
	case PlayerRank::Regular:
		SendString("You have been promoted");
		m_rank = PlayerRank::God;
		return;
	case PlayerRank::God:
		SendString("You have been promoted");
		m_rank = PlayerRank::Admin;
		return;
	case PlayerRank::Admin:
		SendString("You cant be promoted");
		return;
	default:
		SendString("ERROR in promoting");
		return;
	}
}

void Player::sendRoom(const std::string data)
{
	m_room_ptr->sendAllInRoom(cyan + data);//add color for room sending
}

void Player::distractIfPossible()
{
	if (m_room_ptr->getID() == 14 && m_room_ptr->getNPCptr(1)->m_distracted == false)//14 is principle office
	{
		m_room_ptr->getNPCptr(1)->m_distracted = true;
		m_isDistracting = true;
		sendRoom("Principle was distracted by " + m_entity.m_name);
	}
	else
	{
		SendString("Nothing to distract here");
	}
}

void Player::distractionStopped()
{
	SendString("Principle is not distracted anymore");
	Map::getRoomPtrFromId(14)->getNPCptr(1)->m_distracted = false;
	m_isDistracting = false;
}

void Player::throwFireworks()
{
	if (m_room_ptr->getID() == 6) //boys toilet
	{
		Item returnedFromInv = m_inventory.removeItemByName("fireworks");
		
		if (returnedFromInv.getID() != 0)
		{
			SendString("You threw the fireworks down the toilets sure some teacehr will come soon to check it out");
			//remove the teacehr from the room for 30 sec
			NPC teacher = Map::getRoomPtrFromId(14)->removeNPCbyID(2);
			//start timer
			GameLoop::startTeacherTimer();
			std::cout << "techer time started" << std::endl;
			Map::getRoomPtrFromId(6)->addNPC(teacher);
		}
		else
		{
			SendString("Couldnt find item you wanted to use");
		}
	}
}

void Player::unlockGateAndTrain()
{
	if (m_room_ptr->getID() == 2 && m_inventory.contains("main gate key"))
	{
		m_inventory.removeItemByName("main gate key");
		SendString("You opened the main gate which leveled you up");
		m_level.m_curLevel = m_level.m_curLevel + 1;//add level
		m_statPoints += 5; //add stat points
		static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->GotoTrain();
	}
	else
	{
		SendString("You need the main gate key to unlock this gate");
	}
}

void Player::train()
{
	static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->GotoTrain();
}

void Player::aktOnSocialSkill()
{
	int aktNum{ 0 };
	if (m_weapon.getID() != 0)
	{
		if (m_room_ptr->removeEnemyByType(m_weapon.m_socialBonus.m_enemyType))
		{
			sendRoom("After you talked to the " + typeToString(m_weapon.m_socialBonus.m_enemyType) + " they left because you are friendly with them and they dropped some gold for you.");
			m_room_ptr->getMoneyOnFloorAfterKill(20);
		}
		else
		{
			aktNum++;
		}
	}
	else
	{
		SendString("No weapon equipped with social skill");
	}
	
	if (m_armor.getID() != 0)
	{
		if (m_room_ptr->removeEnemyByType(m_armor.m_socialBonus.m_enemyType))
		{
			sendRoom("After you talked to the " + typeToString(m_armor.m_socialBonus.m_enemyType) + " they left because you are friendly with them and they dropped some gold for you.");
			m_room_ptr->getMoneyOnFloorAfterKill(20);
		}
		else
		{
			aktNum++;
		}
	}
	else
	{
		SendString("No armor equipped with social skill");
	}
	if (aktNum == 2)
	{
		SendString("No one who you could talked to in the room.");
	}
}

void Player::whisperByName()
{
	m_room_ptr->whisperToPlayerByName(ParseWord(m_lastData, 1), m_lastData, *this);
}

void Player::printEnemies()
{
	if (!m_room_ptr->m_enemies.empty())
	{
		std::string temp{ red + "ENEMIES\r\n" };
	for (auto & enemy : m_room_ptr->m_enemies)
	{
		temp += enemy.m_enemyStats.m_BSTName;
		temp += " the ";
		temp += enemy.getEntName();
		temp += ": ";
		temp += std::to_string(enemy.m_enemyStats.m_Health.m_currentHealth);
		temp += "HP \r\n";
	}
	SendString(temp);
}
}

void Player::printNPCs()
{
	if (!m_room_ptr->m_NPCsInRoom.empty())
	{
		std::string temp{ red + "NPC CHARACTERS\r\n" };
		for (auto & npc : m_room_ptr->m_NPCsInRoom)
		{
			temp += npc.m_entity.m_name + "\r\n";
		}
		SendString(temp);
	}
}

void Player::printItems()
{
	if (!m_room_ptr->m_items.empty())
	{
	std::string temp{ magenta + "ITEMS\r\n" };
	for (auto & item : m_room_ptr->m_items)
	{
			temp += item.getEntName() + "			buy price $" + tostring(item.m_price.m_buyPrice) + " sell price $" + tostring(item.m_price.m_SellPrice) + "\r\n";
	}
	SendString(temp);
}
}

void Player::printMoney()
{
	std::string temp{ "MONEY$$: " };
	temp += tostring(m_room_ptr->m_money);
	SendString(temp);
}

std::string Player::getRankStr()
{
	switch (m_rank)
	{
	case PlayerRank::Regular:
		return "Regular";
	case PlayerRank::God:
		return "God";
	case PlayerRank::Admin:
		return "Admin";
	default:
		return "No Rank";
	}
}

Parser & Player::getParser()
{
	return static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->getParser();
}

void Player::subtractStatPoint()
{
	m_statPoints--;
}

void Player::addTobaseAttr(int p_attr, int val)
{
	if (p_attr == 0)
	{
		m_baseStats.m_STR++;
	}
	if (p_attr == 1)
	{
		m_baseStats.m_Health.m_maxHealth += 10;
	}
	if (p_attr == 2)
	{
		m_baseStats.m_AGI++;
	}
	//maybe recalc stats
}

void Player::pickupItem()
{
	
		if (m_inventory.haveSpace())
		{
			Item picked = m_room_ptr->removeFirstPickedItem();
			if (picked.getID() != 0)
			{
				m_inventory.addItem(picked);
				SendString("You picked up: " + picked.getEntName());
			}
			else
			{
				SendString("There is nothing to pick up");
			}
		}
		else
		{
			SendString("Inventory full");
		}
	

}

void Player::pickupItemWithName()
{
	if (m_inventory.haveSpace())
	{
		Item picked = m_room_ptr->removeItemByName(ParseWord(m_lastData, 1));
		if (picked.getID() != 0)
		{
			m_inventory.addItem(picked);
			SendString("You picked up: " + picked.getEntName());
		}
		else
		{
			SendString("There is nothing to pick up by that name");
		}
	}
	else
	{
		SendString("Inventory full");
	}
	
}

void Player::dropItemWithName()
{
	if (!m_inventory.m_items.empty())
	{
		Item toRemove = m_inventory.removeItemByName(ParseWord(m_lastData, 1));
		if (toRemove.getID() != 0)
		{
			m_room_ptr->addItem(toRemove);
			SendString("You dropped: " + toRemove.getEntName());
		}
		else
		{
			SendString("There is nothing to drop by that name");
		}
	}
	else
	{
		SendString("Nothing to drop");
	}

}

void Player::buyItem()
{
	if (m_inventory.haveSpace())
	{
		Item picked = m_room_ptr->removeFirstBoughtItem(*this);
		if (picked.getID() != 0)
		{
			m_inventory.addItem(picked);
			m_money = m_money - picked.m_price.m_buyPrice;
			SendString("You bought : " + picked.getEntName());
		}
		else
		{
			SendString("You dont have enough money to buy the item or incorrect name of the item");
		}
	}
	else
	{
		SendString("Inventory full");
	}
}

void Player::buyItemWithName()
{
	if (m_inventory.haveSpace())
	{
		Item picked = m_room_ptr->removeBoughtItemByName(ParseWord(m_lastData, 1),*this);
		if (picked.getID() != 0)
		{
			m_inventory.addItem(picked);
			m_money = m_money - picked.m_price.m_buyPrice;
			sendRoom(m_entity.m_name + +" buys a " + picked.getEntName());
		}
		else
		{
			SendString("You dont have enough money to buy the item or incorrect name of the item");
		}
	}
	else
	{
		SendString("Inventory full");
	}
}

void Player::sellItemWithName()
{
	if (!m_inventory.m_items.empty())
	{
		Item returnedFromInv = m_inventory.removeItemByName(ParseWord(m_lastData, 1));
		if (returnedFromInv.getID() != 0)
		{
			//selling item here
			m_money = m_money + returnedFromInv.m_price.m_SellPrice;
			sendRoom(m_entity.m_name + +" sells a " + returnedFromInv.getEntName() + " for " + tostring(returnedFromInv.m_price.m_SellPrice)+ "$");
		}
		else
		{
			SendString("Couldnt find item you wanted to sell");
		}
	}
}

void Player::attackByName()
{
	m_room_ptr->attackPlayerByName(ParseWord(m_lastData, 1), *this);

	//if (attPlr.getNewbie())//if they don't exist basically
	//{
	//	if (fought(attPlr))
	//	{
	//		attPlr.checkIfDead();
	//	}
	//}
}

bool Player::canBuy(int price)
{
	if (m_money >= price)//can buy
		return true;

	return false;
}


void Player::pickupMoney()
{
	SendString("You picked up: " + tostring(m_room_ptr->m_money)+ "$");
	m_money += m_room_ptr->m_money;
	m_room_ptr->m_money = 0;
}

void Player::changeCommand()
{
	
	findAndChangeCommand(m_lastData);

}

void Player::findAndChangeCommand(std::string data)
{
	//SimpleMUD::GameHandler* = 
	std::string cmd = ParseWord(data, 1);
	std::string shortcut = ParseWord(data, 2);
	if (shortcut.size() < 10)
	{
		SendString("You are changing command " + m_lastData);
		static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->getParser().changeCommand(cmd, shortcut);
	}
	else
	{
		SendString("Shortcut too long");
	}
	//m_connection->Handler()
}

void Player::repeatLastCommand()
{
	std::cout << "going to repeat last command" << m_lastData << std::endl;
	static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->getParser().parseAndExecute(m_lastData, *this);
}

void Player::kickAnotherPlayer()
{
	if (m_rank == PlayerRank::Admin || m_rank == PlayerRank::God)
	{
		Player * plrPtr = OnlinePlayers::getOnlinePlayerByName(ParseWord(m_lastData, 1));
		if (plrPtr == nullptr)
		{
			SendString("Player name not found");
			return;
		}

		if(plrPtr->getRank() == PlayerRank::Admin || plrPtr->getRank() == PlayerRank::God)
		{
			SendString("You cant kick that player");
			return;			
		}	

		OnlinePlayers::sendAllPlayers( bred + m_entity.m_name + " kicked " + plrPtr->getEntName() + " from server" + reset);
		plrPtr->getConn()->Close();
		return;
	}
}

void Player::banAnotherPlayer()
{
	if (m_rank == PlayerRank::Admin)
	{
		std::string name = ParseWord(m_lastData, 1);

		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		int bannedDays{ 0 };
		sql << "select days from bannedplayers where name = :name", into(bannedDays), use(ParseWord(m_lastData, 1));
		if (bannedDays != 0)
		{
			SendString("Player already banned ");
			return;
		}

		Player p;
		sql << "select * from players where entity_name = :entity_name", into(p), use(name);
		if (p.getEntName() == "emptyEntityName")
		{
			SendString("Player is not in database");
			return;
		}

		if (p.getRank() == PlayerRank::Admin)
		{
			SendString("You cant ban that player");
			return;
		}

		
		int days{ 1 };
		sql << "insert into bannedplayers(name, days) "
			"values(:name, :days)", use(name), use(days);
		OnlinePlayers::sendAllPlayers(bred + m_entity.m_name + " banned " + p.getEntName() + " from server" + reset);
		return;
		
	}
}

void Player::removeBanFromPlayer()
{
	if (m_rank == PlayerRank::Admin)
	{
		std::string name = ParseWord(m_lastData, 1);

		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		int bannedDays{ 0 };
		sql << "select days from bannedplayers where name = :name", into(bannedDays), use(ParseWord(m_lastData, 1));
		if (bannedDays == 0)
		{
			SendString("Player not banned ");
			return;
		}

		Player p;
		sql << "select * from players where entity_name = :entity_name", into(p), use(name);
		if (p.getEntName() == "emptyEntityName")
		{
			SendString("Player is not in database");
			return;
		}

		sql << "delete from bannedplayers where name = :name", use(name);
		OnlinePlayers::sendAllPlayers(bred + m_entity.m_name + " removed " + p.getEntName() + "'s ban from server" + reset);
		return;

	}
}

void Player::createParty()
{
	Party pp;
	if (ParseWord(m_lastData, 1) != "")
	{
		pp.m_password = ParseWord(m_lastData, 1);
		auto sp1 = std::make_shared<Party>(pp);
		m_party = sp1;
		//m_party->m_leader = this->getID();
		m_party->addMember(this->getID(), ParseWord(m_lastData, 1));
		SendString("Party created");
		return;
	}
	auto sp1 = std::make_shared<Party>(pp);
	m_party = sp1;
	//m_party->m_leader = this->getID();
	m_party->addMember(this->getID(),"0000");
	SendString("Party created");
}

void Player::sendParty()
{
	if (m_party.get() == nullptr)
	{
		SendString("You are not a member of a party.");
		return;
	}
	 
	m_party->sendParty(byellow + this->getEntName() + ": " + RemoveWord(m_lastData, 0) + reset);
}

void Player::joinParty()
{
	if (m_party != nullptr)
	{
		/*m_party->removeMember(this->getID());
		m_party->sendParty(byellow + this->getEntName() + ": " + " left party" + reset);*/
		SendString("You have to resign the party you are in to be able to join another one");
		return;
	}
	if (OnlinePlayers::getOnlinePlayerByName(ParseWord(m_lastData, 1)) == nullptr)
	{
		SendString("That player is not online");
		return;

	}
	if (OnlinePlayers::getOnlinePlayerByName(ParseWord(m_lastData, 1))->m_party.get() == nullptr)
	{
		SendString("That player is not a leader of a party");
		
	}
	else
	{
		if (OnlinePlayers::getOnlinePlayerByName(ParseWord(m_lastData, 1))->m_party.get()->addMember(this->getID(), ParseWord(m_lastData, 2)))
		{
			SendString("You joined " + ParseWord(m_lastData, 1) + "s party.");
			m_party = OnlinePlayers::getOnlinePlayerByName(ParseWord(m_lastData, 1))->m_party;
			m_lastData = this->getEntName() + " joins the party";
			sendParty();
		}
		else
		{
			SendString("Party is full or your passwor is incorect or you are alredy in the party!");
		}
	}
}

void Player::leaveParty()
{
	if (m_party.get() == nullptr)
	{
		SendString("You are not in the party");
	}
	else
	{
		m_lastData = this->getEntName() + " leaves the party";
		sendParty();
		m_party->removeMember(this->getID());		
		m_party = nullptr;
	}
}


void Player::useItem(Item toUse)
{
	switch (toUse.m_itemType)
	{
	case ItemType::Armour:
		//send to room what hapened
		equipNewArmor(toUse);//need to recalculate the stats
		sendRoom(m_entity.m_name + " equipped " + toUse.getEntName());
		return;
	case ItemType::Weapon:
		equipNewWeapon(toUse);//need to recalculate the stats
		sendRoom(m_entity.m_name + " equipped " + toUse.getEntName());
		return;
	case ItemType::Potion:
		//what health to add from potion check database
		usePotion(toUse);//this could also be a str potion or agi potion
		sendRoom(m_entity.m_name + " used " + toUse.getEntName());
		return;
	case ItemType::Key:
		std::cout << "Need to write code to use key" << std::endl;
		SendString("Need to write code to use key");
		return;
	default:
		std::cout << "Not recogniset item type" << std::endl;
		SendString("Not recogniset item type");
		return;
	}
}

void Player::equipNewArmor(Item toUse)
{
	if (m_armor.getID() != 0)
	{
		removeEquippedArmorBonuses();
		m_inventory.addItem(m_armor);//add armor back to inventory
		m_armor = toUse;
		addEquippedArmorBonuses();
	
	}
	else
	{
		m_armor = toUse;//no armour equipped
		addEquippedArmorBonuses();
	}
}



void Player::equipNewWeapon(Item toUse)
{
	if (m_weapon.getID() != 0)
	{
		removeEquippedWeaponBonuses();
		m_inventory.addItem(m_weapon);//add weapon back to inventory
		m_weapon = toUse;
		addEquippedWeaponBonuses();

	}
	else
	{
		m_weapon = toUse;//no weapon equipped
		addEquippedWeaponBonuses();
	}
}

void Player::usePotion(Item toUse)
{
	m_baseStats.m_Health.m_currentHealth += toUse.m_baseStats.m_Health.m_maxHealth;
}


void Player::removeEquippedArmorBonuses()
{
	m_baseStats.removeAnotherBaseStats(m_armor.m_baseStats);//check the current health interaction	
}

void Player::removeEquippedWeaponBonuses()
{
	m_baseStats.removeAnotherBaseStats(m_weapon.m_baseStats);
}

void Player::addEquippedArmorBonuses()
{
	m_baseStats.addAnotherbaseStats(m_armor.m_baseStats);
}

void Player::addEquippedWeaponBonuses()
{
	m_baseStats.addAnotherbaseStats(m_weapon.m_baseStats);
}



bool Player::ifFoundUse()
{
	
	////std::string REGEX_GET = R"((GET)\s\/(.+)\s(HTTP.+))";
	//for (auto & item : m_inventory.m_items)
	//{
	//	std::string REGEX_GET = R"(\b()" + item.getEntName() + R"(|)" + item.m_baseStats.m_BSTName + R"()\b)";

	//	std::cout << REGEX_GET << std::endl;
	//	std::cout << m_lastData << std::endl;

	//	std::regex rx(REGEX_GET);
	//	std::smatch pieces_match;
	//	if (std::regex_search(m_lastData, pieces_match, rx))
	//	{
	//		std::cout << "ITEM found going to USE IT: " << m_lastData << std::endl;
	//		useItem(item);
	//		return true;
	//	}
	//}
	//return false;
	Item returnedFromInv = m_inventory.removeItemByName(ParseWord(m_lastData, 1));
	if (returnedFromInv.getID()!=0)
	{
		if (returnedFromInv.getID() == 11)
		{
			m_inventory.addItem(returnedFromInv);
			SendString("Fireworks can only be thrown");
			return false;
		}
		else
		{
			useItem(returnedFromInv);
			return true;
		}
	}
	else
	{
		SendString("Couldnt find item you wanted to use");
		return false;
	}
}

bool Player::fightEnemy(Enemy & enemy)
{
	if (checkIfHitEnemy(enemy))
	{
		inflictDamageOnEnemy(enemy, calculateDamageOnEnemy(enemy));

		if(checkIfKillEnemy(enemy))
		{
			return true;
		}
	}
	else
	{
		SendString("You missed!");
	}

	return false;
}

bool Player::checkIfHitEnemy(Enemy & enemy)
{
	if (getRandomMt19937(0, 20) >= 7)
	{
		return true;
	}
	else
	{
		return false;
	}

}

int Player::calculateDamageOnEnemy(Enemy & enemy)
{
	if (m_baseStats.m_STR > 0)
	{
		if (m_baseStats.m_STR == enemy.m_enemyStats.m_STR)
			return m_baseStats.m_STR + getCurrLevel();

		return (m_baseStats.m_STR / enemy.m_enemyStats.m_STR) * m_baseStats.m_STR + getCurrLevel();
	}
	
	return 0 + getCurrLevel();
}

void Player::inflictDamageOnEnemy(Enemy & enemy, int damage)
{
	enemy.m_enemyStats.m_Health.m_currentHealth -= damage;
	SendString("You have hit " + enemy.m_enemyStats.m_BSTName + " for " + std::to_string(damage) + " health.");
}

bool Player::checkIfKillEnemy(Enemy & enemy)
{
	if (enemy.m_enemyStats.m_Health.m_currentHealth <= 0)
		return true;

	return false;
}

bool Player::checkIfDead()
{
	if (m_baseStats.m_Health.m_currentHealth <= 0)
	{
		OnlinePlayers::sendAllPlayers(red + getEntName() + " was knocked out! They woke up in the Common Room.");
		respawn();
		return true;
	}

	return false;
}

void Player::respawn()
{
	m_baseStats.m_Health.m_currentHealth = m_baseStats.m_Health.m_maxHealth;
	getRoomRef().removePlayer(getID());
	setRoomPtr(Map::getRoomPtrFromId(1));
	setRoomID(1);
	getRoomRef().storePlayer(getID());
	printCurrRoom();
}

void Player::leaveGame()
{
	//static_cast<SimpleMUD::GameHandler*>(m_connection->Handler())->Leave();

	m_connection->Close();
}

void Player::SendString(const std::string & toSend)
{
	if (m_connection == 0)
	{
		ERRORLOG.Log("Trying to send string to player " +
			m_entity.m_name + " but player is not connected.");
		return;
	}

	// send the string, newline included.
	m_connection->Protocol().SendString(*m_connection, toSend + Lukas::newline);

	//i know the player is active because i only have active players logged in
	/*if (Active())
	{*/
	//PrintStatbar();
	/*}*/
}

void Player::PrintStatbar(bool p_update)
{
	
	// if this is a statusbar update and the user is currently typing something,
	// then do nothing.
	if (p_update && m_connection->Protocol().Buffered() > 0)
		return;

	std::string statbar = white + bold + "[";

	int ratio = 100 * getCurrHealth() / getMaxHealth();

	// color code your hitpoints so that they are red if low,
	// yellow if medium, and green if high.
	if (ratio < 33)
		statbar += red;
	else if (ratio < 67)
		statbar += yellow;
	else
		statbar += green;

	statbar += tostring(getCurrHealth()) + white + "/" +
		tostring(getMaxHealth()) + "] ";

	m_connection->Protocol().SendString(*m_connection, clearline + "\r" + statbar + "Online id: " + tostring(getID()) + reset);


}

void Player::printRoomFromPtr()
{
	m_connection->Protocol().SendString(*m_connection, clearline + "\r" + "THIS IS THE COMMING ROOM DESCRIPTION FROM THE PLAYERS POINTER" + reset);

}

void Player::testFunc()
{
	std::cout << "Test Func worked for Player" << std::endl;
}

bool Player::fought(Player & plr)
{
	if (checkIfWasHit(plr))
	{
		getDamaged(plr, calcYourDmg(plr));

		if (checkIfKilled(plr))
		{
			return true;
		}
	}
	else
	{
		plr.SendString("You missed!");
	}

	return false;
}

bool Player::checkIfWasHit(Player & plr)
{
	if (getRandomMt19937(0, 13 + 7) >= 7)
	{
		return true;
	}

	return false;
}

int Player::calcYourDmg(Player & plr)
{
	if (plr.getSTR() > 0)
	{
		if (plr.getSTR() == getSTR())
			return plr.getSTR() + plr.getCurrLevel();

		return (plr.getSTR() / getSTR()) * plr.getSTR() + plr.getCurrLevel();
}

	return 0 + plr.getCurrLevel();
}

void Player::getDamaged(Player & plr, int damage)
{
	setCurrHealth(getCurrHealth() - damage);
	plr.SendString("You have hit " + getEntName() + " for " + std::to_string(damage) + " health.");
	SendString(plr.getEntName() + " hit you for " + std::to_string(damage) + " health.");
}

bool Player::checkIfKilled(Player & plr)
{
	if (getCurrHealth() <= 0)
		return true;

	return false;
}

void Player::drawMap()
{
	SendString(Lukas::red + "\n===================================\n");
	SendString(Lukas::white + " +---+         +---+  +---+");
	SendString(Lukas::white + " | " + Lukas::red + "A" + Lukas::white + " |---------| " + Lukas::red + "B" + Lukas::white + " |--| " + Lukas::red + "C" + Lukas::white + " |");
	SendString(Lukas::white + " +---+         +---+  +---+");
	SendString(Lukas::white + "   |             |");
	SendString(Lukas::white + "   |    +---+  +---+  +---+");
	SendString(Lukas::white + "   |    | " + Lukas::red + "D" + Lukas::white + " |--| " + Lukas::red + "E" + Lukas::white + " |--| " + Lukas::red + "F" + Lukas::white + " |");
	SendString(Lukas::white + "   |    +---+  +---+  +---+");
	SendString(Lukas::white + "   |             |      |");
	SendString(Lukas::white + "   |    +---+    |    +---+");
	SendString(Lukas::white + "   |    | " + Lukas::red + "G" + Lukas::white + " |    |    | " + Lukas::red + "H" + Lukas::white + " |");
	SendString(Lukas::white + "   |    +---+    |    +---+");
	SendString(Lukas::white + "   |      |      |");
	SendString(Lukas::white + " +---+  +---+  +---+  +---+  +---+");
	SendString(Lukas::white + " | " + Lukas::red + "I" + Lukas::white + " |--| " + Lukas::red + "J" + Lukas::white + " |--| " + Lukas::red + "K" + Lukas::white + " |--| " + Lukas::red + "L" + Lukas::white + " |--| " + Lukas::red + "M" + Lukas::white + " |");
	SendString(Lukas::white + " +---+  +---+  +---+  +---+  +---+");
	SendString(Lukas::white + "          |      |");
	SendString(Lukas::white + "        +---+  +---+");
	SendString(Lukas::white + "        | " + Lukas::red + "N" + Lukas::white + " |  | " + Lukas::red + "O" + Lukas::white + " |");
	SendString(Lukas::white + "        +---+  +---+");
	SendString(Lukas::red + "\n===================================\n");
	SendString(Lukas::red + "A: " + Lukas::white + "Football Pitch");
	SendString(Lukas::red + "B: " + Lukas::white + "Backyard");
	SendString(Lukas::red + "C: " + Lukas::white + "Playground (Shop)");
	SendString(Lukas::red + "D: " + Lukas::white + "Maths Room");
	SendString(Lukas::red + "E: " + Lukas::white + "Centre Hall");
	SendString(Lukas::red + "F: " + Lukas::white + "Library (Shop)");
	SendString(Lukas::red + "G: " + Lukas::white + "Boy's Toilet");
	SendString(Lukas::red + "H: " + Lukas::white + "Teacher's Lounge");
	SendString(Lukas::red + "I: " + Lukas::white + "Gym");
	SendString(Lukas::red + "J: " + Lukas::white + "Locker Room");
	SendString(Lukas::red + "K: " + Lukas::white + "Front Hall");
	SendString(Lukas::red + "L: " + Lukas::white + "Canteen");
	SendString(Lukas::red + "M: " + Lukas::white + "Kitchen");
	SendString(Lukas::red + "N: " + Lukas::white + "Girl's Toilet");
	SendString(Lukas::red + "O: " + Lukas::white + "Front Gate");
	SendString(Lukas::red + "\n===================================\n");
}

void Player::levelUpCheck()
{
	if (getCurrExp() >= getNextLevel())
	{
		setCurrLevel(getCurrLevel() + 1);
		std::cout << "Curr Level: " << getCurrLevel() << std::endl;
		setCurrExp(getCurrExp() - getNextLevel());
		std::cout << "Curr Exp: " << getCurrExp() << std::endl;
		setNextLevel(getNextLevel() * getCurrLevel());
		std::cout << "Next Level: " << getNextLevel() << std::endl;

		SendString("You have Leveled Up! You are now on Level " + std::to_string(getCurrLevel()) + "! " +  std::to_string(getNextLevel()) + "Exp until next Level.");
	}
	else
	{
		SendString("You can't level up yet. " + std::to_string(getNextLevel() - getCurrExp()) + "Exp until next Level.");
	}
}