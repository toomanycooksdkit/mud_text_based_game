#pragma once
#include <string>

struct RoomExits
{
	int m_north{ 0 };
	int m_east{ 0 };
	int m_south{ 0 };
	int m_west{ 0 };

	std::string availableExits();
};