#pragma once

struct Health
{
	int m_currentHealth{ 100 };
	int m_regenAmount{ 0 };
	int m_maxHealth{ 100 };

	void addAnotherHealth(Health & another);
	void removeAnotherHealth(Health & another);
};