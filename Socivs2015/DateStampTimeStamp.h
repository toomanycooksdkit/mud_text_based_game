#pragma once

#include<string>
#include <ctime>
// ============================================================
// This prints a timestamp in 24 hours hh:mm:ss format
// ============================================================
std::string TimeStamp();


// ============================================================
// This prints a datestamp in YYYY:MM:DD format
// ============================================================
std::string DateStamp();