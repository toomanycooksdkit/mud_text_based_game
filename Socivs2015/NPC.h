#pragma once
#include "Entity.h"
#include <string>

class NPC
{
private:
	
public:
	Entity m_entity;
	bool m_distracted{ false };

	NPC() = default;
	NPC(std::string name, int id);
};

