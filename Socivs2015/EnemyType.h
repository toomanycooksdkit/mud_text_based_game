#pragma once
#include <string>

//c++11 the new enum class you can assign the walues as you wish for example troll can start at 8 or 20 whatever you want
enum class EnemyType
{
	Troll,
	Witch,
	Dragon,
	Skeleton,
	Ghost
};

std::string typeToString(const EnemyType & type);
