#include "CommandFunctions.h"
#include "BasicLibString.h"
#include "GameHandler.h"
#include "Map.h"
#include "soci.h"
#include "soci-postgresql.h"
#include "DateStampTimeStamp.h"
#include "GameLoop.h"
#include <chrono>

using namespace Lukas;
using namespace BasicLib;
using namespace SimpleMUD;

void printPlayerStats(Player & plr)
{
	plr.printStats();
}

void chatToAll(Player & plr)
{
	std::string text = RemoveWord(plr.m_lastData, 0);
	OnlinePlayers::sendAllPlayers(magenta + bold + plr.getEntName() + " chats: " + white + text);
}

void sayToRoom(Player & plr)
{
	plr.sendRoom(BasicLib::RemoveWord(plr.m_lastData, 0));
}

void shutdownServer(Player & plr)
{
	if (plr.getRank() == PlayerRank::Admin)
	{
		announceToAll("SYSTEM IS SHUTTING DOWN");
		GameHandler::Running() = false;
	}
	else
	{
		plr.SendString("You do not have the permission to do this!");
	}
	
}

void move(Player & plr, int id, std::string msg)
{
	if (id <= 0)
	{
		plr.SendString(msg);
	}
	else
	{
		plr.getRoomRef().removePlayer(plr.getID());
		plr.getRoomRef().printRoom();
		plr.setRoomPtr(Map::getRoomPtrFromId(id));
		plr.setRoomID(id);
		//generate enemies for room

		plr.getRoomRef().ifEmptyGenerateEnemies();

		plr.getRoomRef().storePlayer(plr.getID());
		plr.printCurrRoom();
	}
}

void goNorth(Player & plr)
{
	move(plr, plr.getRoomRef().m_exits.m_north, "You can't go North.");

	testTemplate(plr);
}

void goSouth(Player & plr)
{
	move(plr, plr.getRoomRef().m_exits.m_south, "You can't go South.");
}

void goEast(Player & plr)
{
	move(plr, plr.getRoomRef().m_exits.m_east, "You can't go East.");
}

void goWest(Player & plr)
{
	move(plr, plr.getRoomRef().m_exits.m_west, "You can't go West.");
}

void testTemplate(Player & plr)
{
	Player p;
	Enemy e;

	plr.testTemp(p);
	plr.testTemp(e);
}

void whisper(Player &plr)
{
	plr.whisperByName();

	//plr.SendString("You whispered. No one heard it, it was a whisper.");
}

void pickItem(Player & plr)
{
		if (BasicLib::ParseWord(plr.m_lastData, 1) == "")//no item name provided
		{
			if (plr.getRoomRef().m_roomType == RoomType::Shop)
			{
				plr.SendString("You can only buy items in shop");
			}
			else
			{
				plr.pickupItem();
			}
			
		}
		else
		{
			if (plr.getRoomRef().m_roomType == RoomType::Shop)
			{
				plr.SendString("You can only buy items in shop");
			}
			else
			{
				plr.pickupItemWithName();
			}
		}
}

void dropItem(Player & plr)
{
	plr.dropItemWithName();
}

void buyItem(Player & plr)
{
	if (BasicLib::ParseWord(plr.m_lastData, 1) == "")//no item name provided
	{
		if (plr.getRoomRef().m_roomType == RoomType::Shop)
		{
			plr.buyItem();
		}
		else
		{
			plr.SendString("You can only buy items in shop");
		}

	}
	else
	{
		if (plr.getRoomRef().m_roomType == RoomType::Shop)
		{
			plr.buyItemWithName();
		}
		else
		{
			plr.SendString("You can only buy items in shop");
		}
	}
}

void sellItem(Player & plr)
{
	if (plr.getRoomRef().m_roomType == RoomType::Shop)
	{
		plr.sellItemWithName();
	}
	else
	{
		plr.SendString("You can only sell items in shop");
	}
}

void useItem(Player & plr)
{
	plr.ifFoundUse();//we can check here it returns bool
}

void help(Player & plr)
{
	plr.printHelp();
}

void attack(Player & plr)
{
	if (BasicLib::ParseWord(plr.m_lastData, 1) == "") //no player name provided
	{
		//fight the enemy here
		if (!plr.getRoomRef().m_enemies.empty())
		{
			if (plr.fight(plr.getRoomRef().m_enemies.back()))
			{
				plr.SendString("You knocked out: " + plr.getRoomRef().m_enemies.back().m_enemyStats.m_BSTName);//fix this here canot get the back if no enemies there
				plr.getRoomRef().getItemsOnFloorAfterKill(ItemHandler::generateRoomItemsAfterKill(3));
				plr.getRoomRef().getMoneyOnFloorAfterKill(ItemHandler::generateMoneyAfterKill());
				plr.getRoomRef().m_enemies.pop_back();
				plr.setCurrExp(plr.getCurrExp() + 10);
				plr.SendString("You gained 10Exp.");
			}
		}
		else
		{
			plr.SendString("There is nothing to attack here.");
		}
	}
	else
	{
		plr.attackByName();
	}
}

void whoIsOnline(Player & plr)
{
	plr.printOnlinePlayers();
}

void whoIsInDatabase(Player & plr)
{
	using namespace soci;
	session sql(postgresql, "dbname=GameDB user=postgres password=password");

	int count;
	sql << "select count(*) from players", into(count);

	std::string pNames{ "ALL PLAYERS IN DATABASE\r\n" };
	std::string pName{ "empty" };
	for (int i = 0; i <= count - 1; i++)
	{
		sql << "select entity_name from players where entity_id = :entity_id", into(pName), use(i);
		pNames += pName + "\r\n";
	}

	plr.SendString(pNames);

}

void promotePlayer(Player & plr)
{
	plr.changeRankOfAnotherPlayer();
}

void demotePlayer(Player & plr)
{
	plr.changeRankOfAnotherPlayerDown();
}

void announceToAll(const std::string & msg)
{
	OnlinePlayers::sendAllPlayers(cyan + bold + "System Announcement: " + msg);
}

void lookAtRoom(Player & plr)
{
	plr.printCurrRoom();
}

void getMoney(Player & plr)
{
	plr.pickupMoney();
}

void changeCommand(Player & plr)
{
	plr.changeCommand();
}

void distract(Player & plr)
{
	plr.distractIfPossible();
}

void fight(Player & plr)
{
	//fight the another payer in room
	if (!plr.getRoomRef().m_playersInRoom.empty())
	{
		
	}
	else
	{
		plr.SendString("There is noone to fight here.");
	}
}

void getTime(Player & plr)
{
	plr.SendString(bold + bgreen + 
		"The current system time is: " + TimeStamp() +
		" on " + DateStamp() +
		"\r\nThe system has been up for: "
		+ tostring(std::chrono::duration_cast<std::chrono::minutes>(GameLoop::a_clock::now().time_since_epoch()).count() - GameLoop::getStartPoint()) + " minutes.");
}

void throwFirewrks(Player & plr)
{
	plr.throwFireworks();
}

void unlockGate(Player & plr)
{
	plr.unlockGateAndTrain();
}

void steal(Player & plr)
{
	if (plr.getRoomRef().getID() == 14)//you need the principle to be distracted
	{
		if (plr.getRoomRef().getNPCptr(1)->m_distracted == true)
		{
			if (plr.getRoomRef().getNPCptr(2) == nullptr)//no teacher in room
			{
				if (BasicLib::ParseWord(plr.m_lastData, 1) == "")//no item name provided
				{
					plr.pickupItem();
					OnlinePlayers::sendAllPlayers(green + "The Main Gate Key Was stolen by: " + plr.getEntName());
				}
				else
				{
					plr.pickupItemWithName();
					OnlinePlayers::sendAllPlayers(green + "The Main Gate Key Was stolen by: " + plr.getEntName());
				}
			}
			else
			{
				plr.SendString("You cant steal the Key while Teacher is watching");
			}
		}
		else
		{
			plr.SendString("You cant steal the Key while principal is watching");
		}
	}
}

void plr_leaveGame(Player & plr)
{
	plr.leaveGame();
}

void repeatLastCommand(Player & plr)
{
	plr.repeatLastCommand();
}

void kickPlayer(Player & plr)
{
	plr.kickAnotherPlayer();
}

void banPlayer(Player & plr)
{
	plr.banAnotherPlayer();
}

void removeBanFromPlayer(Player & plr)
{
	plr.removeBanFromPlayer();
}

void optInOutPVP(Player &plr)
{
	plr.setPvp(!plr.getPvp());
	std::cout << "PVP was changed to " << plr.getPvp() << std::endl;

	if (plr.getPvp())
	{
		OnlinePlayers::sendAllPlayers(plr.getEntName() + " is ready for a fight!");
	}
	else
	{
		OnlinePlayers::sendAllPlayers(plr.getEntName() + " chickened out and won't fight anyone!");
	}
}

void trainPlayer(Player & plr)
{
	plr.train();
}

void aktOnSocialSkill(Player & plr)
{
	plr.aktOnSocialSkill();
}


void createParty(Player & plr)
{
	plr.createParty();
}

void sendParty(Player & plr)
{
	plr.sendParty();
}

void joinParty(Player & plr)
{
	plr.joinParty();
}

void leaveParty(Player & plr)
{
	plr.leaveParty();
}

void drawMap(Player &plr)
{
	plr.drawMap();
}

void levelUp(Player &plr)
{
	plr.levelUpCheck();
}