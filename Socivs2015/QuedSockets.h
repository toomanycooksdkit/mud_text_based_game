#pragma once

#include<thread>
#include<iostream>
#include<mutex>

#include"ConnectionWithThread.h"
#include"ConnectionManager.h"

namespace Lukas
{
//class which synchronize acces to the worker threads working on the processing the individual sockets
class QuedSockets
{
private:

	std::vector<DataSocket> m_WorkVector;
	std::mutex m_VecMut;
public:

	void add(DataSocket& item)
	{
		std::lock_guard<std::mutex> lg(m_VecMut);
		m_WorkVector.push_back(item);
		//there will be notify.one() to wake up one sleeping thread
	}


	//DataSocket& remove() this version was introducing datarace as compiler bpobably just returned address of the vector because it optimized the code
	DataSocket remove()
	{
		
		std::lock_guard<std::mutex> lg(m_VecMut);
		DataSocket toRet;
		//std::cout << "remove locking" << std::endl;
		toRet = m_WorkVector.back();
		m_WorkVector.pop_back();	
		//std::cout << "remove unlocking" << std::endl;
		return toRet;
	}

	bool Empty()
	{
		std::lock_guard<std::mutex> lg(m_VecMut);
		//std::cout << "empty locking" << std::endl;
		return m_WorkVector.empty();
		//std::cout << "empty unlocking" << std::endl;
	}
};


}