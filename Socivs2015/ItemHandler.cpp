#include "ItemHandler.h"

std::vector<Item> m_PossibleItems;

using namespace soci;

void ItemHandler::loadItems()
{
	std::cout << "Loading Items" << std::endl;

	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	int count;
	sql << "select count(*) from items", into(count);


	m_PossibleItems.reserve(count);
	Item item;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from items where entity_id = :entity_id", into(item), use(i);
		m_PossibleItems.emplace_back(item);
	}

	std::cout << "Items Loaded" << std::endl;

}

void ItemHandler::printItems()
{
	for (const auto& item : m_PossibleItems)
	{
		item.printItem();
	}
}

std::list<Item> ItemHandler::generateRoomItemsAfterKill(int number)
{
	std::list<Item> toRetItems;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector
	for (int i = 0; i < number; i++)
	{
		generatedNum = getRandomMt19937(0, m_PossibleItems.size() - 1);
		toRetItems.push_back(m_PossibleItems[generatedNum]);
	}

	return toRetItems;
}

int ItemHandler::generateMoneyAfterKill()
{
	return getRandomMt19937(10, 30);
}

Item ItemHandler::getItemFromID(int id)
{
	Item toRet;
	if (id == 0 || id == 11 || id == 14)//banned ids as they are not in DB yet
	{
		return toRet;
	}
	else
	{
		return m_PossibleItems[id - 1]; //fast acces
	}
}
