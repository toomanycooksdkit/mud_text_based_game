#include"Inventory.h"
#include"BasicLibString.h"
#include "ItemHandler.h"

bool Inventory::haveSpace()
{
	for (auto & item : m_items)
	{
		if (item.getID() == 0)
		{
			std::cout << "have space" << std::endl;
			return true;
		}
	}
	std::cout << "Inv Full" << std::endl;
	return false;
}

bool Inventory::contains(const std::string & toCheck)
{
	if (!m_items.empty())
	{
		for (auto it = m_items.begin(); it != m_items.end(); it++)
		{
			if (it->getID() != 0 && BasicLib::LowerCase(it->getEntName()) == BasicLib::LowerCase(toCheck))
			{		
				return true;
			}

		}
	}
	return false;//empty item if didnt find one in the room
}

bool Inventory::addItem(Item toAdd)
{
	
	for (auto & item : m_items)
	{
		if (item.getID() == 0)
		{
			std::cout << "Item added" << std::endl;
			item = toAdd;
			return true;
		}
	}
	std::cout << "Inv Full" << std::endl;
	return false;
}

Item Inventory::removeItemByName(std::string toRemove)
{
	Item toRet;//initialized empty
	if (!m_items.empty())
	{
		for (auto it = m_items.begin(); it != m_items.end(); it++)
		{
			if (it->getID() != 0 && BasicLib::LowerCase(it->getEntName()) == BasicLib::LowerCase(toRemove))
			{
				toRet = *it;
				std::cout << "erasing " << std::endl;
				it->setID(0);//set id to 0 when removed
				return toRet;
			}

		}
	}
	return toRet;//empty item if didnt find one in the room
}

//we could use select statements directly from database
void Inventory::getItemsFromIds()
{
	std::cout << "Getting actual items from ids" << std::endl;
	for (auto& itm : m_items)
	{
		itm = ItemHandler::getItemFromID(itm.getID());
	}
}
