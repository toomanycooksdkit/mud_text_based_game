#pragma once

#include <string>
#include"Connection.h"
#include"Telnet.h"

using std::string;
using Lukas::Telnet;
using SocketLib::Connection;

namespace SimpleMUD
{


	class Train : public Telnet::handler
	{
		// typedef the class because MSVC6 isn't smart enough to construct
		// a "Telnet::handler" object, yet it will construct a typedef just
		// fine.  *boggle*
		typedef Telnet::handler thandler;
	public:

		// ------------------------------------------------------------------------
		//  Construct the handler with a reference to the connection so that it can
		//  be used later on. Handlers are initialized only once, and cannot change
		//  connections. A flaw in MSVC6 neccessitated the typedeffing of
		//  Telnet::handler to thandler in order to call the base constructor.
		// ------------------------------------------------------------------------
		Train(Connection<Telnet>& p_conn, int playerID)
			: thandler(p_conn)
		{
			m_playerID = playerID;
		}

		// ------------------------------------------------------------------------
		//  standard handler commands
		// ------------------------------------------------------------------------
		void Handle(string p_data);
		void Enter();
		void Leave() {}
		void Hungup() { /*PlayerDatabase::Logout(m_player);*/ } //need to be adapted to our database
		void Flooded() { /*PlayerDatabase::Logout(m_player);*/ }

		// ------------------------------------------------------------------------
		//  This function prints out your statistics.
		// ------------------------------------------------------------------------
		void PrintStats(bool p_clear = true);

	protected:
		int m_playerID;
	};  // end class Train



}   // end namespace SimpleMUD
