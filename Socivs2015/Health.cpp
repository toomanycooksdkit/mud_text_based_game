#include "Health.h"

void Health::addAnotherHealth(Health & another)
{
	m_currentHealth += another.m_currentHealth;
	m_maxHealth += another.m_maxHealth;
	m_regenAmount += another.m_regenAmount;
}

void Health::removeAnotherHealth(Health & another)
{
	//can add checks for negative values
	m_currentHealth -= another.m_currentHealth;
	m_maxHealth -= another.m_maxHealth;
	m_regenAmount -= another.m_regenAmount;
}
