#pragma once

#include <vector>
#include <algorithm>
#include <random>

#include "Item.h"
#include"HelperFunctions.h"
#include "soci.h"
#include "soci-postgresql.h"
#include<list>

//std::random_device m_Rd; https://msdn.microsoft.com/en-us/library/bb982398.aspx
//std::uniform_int_distribution<int> dist();
//std::mt19937 m_Mt;


//could be std::array if know size at advance //http://stackoverflow.com/questions/4424579/stdvector-versus-stdarray-in-c


class ItemHandler
{
private:

public:
	static void loadItems();
	static void printItems();
	static std::list<Item> generateRoomItemsAfterKill(int number);
	static int generateMoneyAfterKill();
	static Item getItemFromID(int id);
};