#pragma once
#include <vector>
#include"Room.h"
#include "soci.h"
#include "soci-postgresql.h"

class Map
{
private:

public:	
	static void loadRooms();
	static void saveRooms();
	static void printMap();
	static Room * getRoomPtrFromId(int roomID);
	static void allEnemyAttacks();
	static void stockAllRooms();
};
