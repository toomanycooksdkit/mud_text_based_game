#include "Party.h"
#include "AllOnlinePlayers.h"

bool Party::addMember(int plrID, const std::string & pass)
{
	if (m_password != pass)
	{
		return false;
	}

	for (auto & member : m_members)
	{
		if (member == -1)
		{
			member = plrID;
			std::cout << "added" << std::endl;
			return true;
		}
		if (member == plrID)
		{
			std::cout << "already in party" << std::endl;
			return false;
		}
	}
	std::cout << "full" << std::endl;
	return false;
}

bool Party::removeMember(int plrID)
{
	for (auto & member : m_members)
	{
		if (member == plrID)
		{
			std::cout << "removed" << std::endl;
			member = -1;
			return true;
		}
	}	
	std::cout << "member not in party" << std::endl;
	return false;
}

void Party::sendParty(const std::string & msg)
{
	for (auto & member : m_members)
	{
		if (member == -1)
		{			
		}
		else
		{
			OnlinePlayers::getOnlinePlayer(member).SendString(msg);
		}
	}
}
