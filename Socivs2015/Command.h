#pragma once

#include <string>
#include <functional>
#include <regex>
#include <iostream>
#include "Player.h"

class Command
{
private:
	std::string m_Command{ "emptyCommand" };
	std::string m_Shortcut{ "emptyShortcut" };
	std::string m_Description{ "emptyDescription" };
	std::function<void(Player&)> m_ExecuteCommand;
public:

	Command() = default;
	Command(std::string cmd,std::string sh,std::function<void(Player&)> fToExecute);
	
	//getters setters
	inline std::string getCommandStr()const { return m_Command; };
	inline void setCommandStr(const std::string& toSet) { m_Command = toSet; };
	inline std::string getShortcut()const { return m_Shortcut; };
	inline void setShortcut(const std::string& toSet) { m_Shortcut = toSet; };
	inline std::string getDescription()const { return m_Description; };
	inline void setDescription(const std::string& toSet) { m_Description = toSet; };
	inline std::function<void(Player&)> getFunc()const { return m_ExecuteCommand; };
	inline void setFunc(std::function<void(Player&)> toSet) { m_ExecuteCommand = toSet; };

	//aditional functions
	void executeCommand(Player& pTOSend);
	bool isCommandInSentence(const std::string sentence);
	bool ifFoundExecute(const std::string toParse, Player& plr);
};

