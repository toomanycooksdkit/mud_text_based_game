
#include"ConnectionManagerNoThreads.h"
#include"ListenningManagerNoThread.h"
#include"SocketLibIncludes_Types.h"
#include "SocketLibSystemWord.h"
#include "Telnet.h"
#include "GameLogonHandler.h"
#include "GameHandler.h"
#include"CommandFunctions.h"
#include"Parser.h"
#include"Command.h"
#include <memory>
#include <functional>
#include"GameLoop.h"
#include"Map.h"
#include "soci.h"
#include "soci-postgresql.h"
#include "Party.h"


int main()
{
	using namespace soci;
	using namespace Lukas;
	using namespace BasicLib;
	using namespace SocketLib;
	using namespace SimpleMUD;

	try
	{	
		SystemWord WSAS;
		GameLoop gameLoop;
		gameLoop.initialize();
		gameLoop.LoadDatabases();
		//Map::printMap();

		ListeningManager<Telnet, Logon> lm;
		ConnectionManager<Telnet, Logon> cm(128, 60, 65536);

		lm.SetConnectionManager(&cm);
		lm.AddPort_S(5099);

		while (GameHandler::Running())
		{
			lm.Listen();
			cm.Manage();
			gameLoop.Loop();
		}
	}
	catch (const std::exception&)
	{

	}

	system("PAUSE");
	return 0;
}