#include "Enemy.h"
#include "Player.h"


Enemy::Enemy(Entity entity, BaseStats baseStats, int socialSkill, EnemyType type)
	:m_entity(entity), m_enemyStats(baseStats), m_socialSkill(socialSkill), m_type(type)
{
}

void Enemy::printEnemy() const
{
	m_entity.printEntity();
}

bool Enemy::attack(Player &plr)
{
	if (checkIfHitPlayer(plr))
	{
		inflictDamageToPlayer(plr, calculateDamageToPlayer(plr));

		if (plr.checkIfDead())
		{
			return true;
		}

		return false;
		
	}
}

bool Enemy::checkIfHitPlayer(Player & plr)
{
	if (getRandomMt19937(0, 20) >= 7)
	{
		return true;
	}

	return false;
}

int Enemy::calculateDamageToPlayer(Player & plr)
{
	if (m_enemyStats.m_STR > 0)
	{
		if (m_enemyStats.m_STR == plr.getSTR())
			return m_enemyStats.m_STR;

		if ((m_enemyStats.m_STR / plr.getSTR()) * m_enemyStats.m_STR == 0)
		{
			return 1;
		}

		return (m_enemyStats.m_STR / plr.getSTR()) * m_enemyStats.m_STR;
	}

	return 1;
}

void Enemy::inflictDamageToPlayer(Player & plr, int damage)
{
	plr.setCurrHealth(plr.getCurrHealth() - damage);
	plr.SendString(m_enemyStats.m_BSTName + " hit you for " + std::to_string(damage) + " health.");
}

void Enemy::testFunc()
{
	std::cout << "Test Func worked for Enemy" << std::endl;
}

bool Enemy::fought(Player & plr)
{
	if (checkIfWasHit(plr))
	{
		getDamaged(plr, calcYourDmg(plr));

		if (checkIfKilled(plr))
		{
			return true;
		}
	}
	else
	{
		plr.SendString("You missed!");
	}

	return false;
}

bool Enemy::checkIfWasHit(Player & plr)
{
	if (getRandomMt19937(0, 20) >= 7)
	{
		return true;
	}

	return false;
}

int Enemy::calcYourDmg(Player & plr)
{
	if (plr.getSTR() > 0)
	{
		if (plr.getSTR() == m_enemyStats.m_STR)
			return plr.getSTR();

		return (plr.getSTR() / m_enemyStats.m_STR) * plr.getSTR();
	}

	return 1;
}

void Enemy::getDamaged(Player & plr, int damage)
{
	m_enemyStats.m_Health.m_currentHealth -= damage;
	plr.SendString("You have hit " + m_enemyStats.m_BSTName + " for " + std::to_string(damage) + " health.");
}

bool Enemy::checkIfKilled(Player & plr)
{
	if (m_enemyStats.m_Health.m_currentHealth <= 0)
	{
		return true;
	}

	return false;
}
