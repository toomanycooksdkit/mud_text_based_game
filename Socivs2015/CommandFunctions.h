#pragma once
#include"Player.h"
#include"AllOnlinePlayers.h"
#include"Telnet.h"
#include"ItemHandler.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Whatewer Paddy was talking in screencast + his commits was his work and whatever Lukas was talking + his commint was his work
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//main functions
void printPlayerStats(Player & plr); //stats

void chatToAll(Player & plr); //chat

void sayToRoom(Player & plr); //say in room

void shutdownServer(Player & plr); //shut

//PADDY
void move(Player & plr, int id, std::string msg);

void goNorth(Player & plr); //Go North
//PADDY
void goSouth(Player & plr); //Go South
//PADDY
void goEast(Player & plr); //Go East
//PADDY
void goWest(Player & plr); //Go West

void testTemplate(Player & plr);

void whisper(Player &plr); //whisper

//LUKAS
void pickItem(Player& plr); //pickup

void dropItem(Player& plr); //drop

void buyItem(Player& plr); //buy

void sellItem(Player & plr); //sell


void useItem(Player& plr); //use

void help(Player & plr); //help
//PADDY
void attack(Player &plr); //attack

//LUKAS
void whoIsOnline(Player& plr); //who
//LUKAS
void whoIsInDatabase(Player& plr); //whoall
//LUKAS
void promotePlayer(Player& plr); //promote <name>
//LUKAS
void demotePlayer(Player& plr); //demote <name>
//helper functions
void announceToAll(const std::string& msg);

void lookAtRoom(Player& plr); //look

//LUKAS
void getMoney(Player& plr); //get
//LUKAS
void changeCommand(Player& plr); //change

void distract(Player& plr); //distract the teacher

void fight(Player & plr); //fight antother player

void getTime(Player & plr); //gets the system time

void throwFirewrks(Player & plr); //throws the fireworks

void unlockGate(Player & plr); //unlock the gate and go to train

void steal(Player & plr); //steal the key from principals office

void plr_leaveGame(Player & plr); //leave the game

void repeatLastCommand(Player & plr);// "/"repeat last command

void kickPlayer(Player & plr); //kick the player

void banPlayer(Player & plr); //ban the player

void removeBanFromPlayer(Player & plr); // unban player

void createParty(Player & plr); //create party

void sendParty(Player & plr); //sends message to the party

void joinParty(Player & plr); // join party

void leaveParty(Player & plr); //leave

void drawMap(Player & plr);

void optInOutPVP(Player & plr); //Opt in and out of the PvP fighting

void trainPlayer(Player & plr); //train 

void aktOnSocialSkill(Player & plr); //akt

void levelUp(Player &plr);