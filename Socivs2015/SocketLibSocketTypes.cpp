#include "SocketLibSocketTypes.h"
#include "SocketLibErrors.h"

namespace Lukas
{
	using namespace SocketLib;
	////////////////////////////////////////////////////BASE SOCKET

	Socket::Socket(sock socket)
		: m_Socket(socket)
	{
		if (socket != INVALID_SOCKET)
		{
			socklen_t s = sizeof(m_LocalInfo);
			getsockname(socket, (sockaddr*)(&m_LocalInfo), &s);
		}

		// the socket is blocking by default
		m_IsBlocking = true;
	}

	Socket::~Socket()
	{
		//maybe close the socket if not already closed
	}

	void Socket::Close()
	{
		closesocket(m_Socket);
		m_Socket = INVALID_SOCKET;
	}

	void Socket::SetBlocking(bool blockMode)
	{

		// If iMode = 0, blocking is enabled; 
		// If iMode != 0, non-blocking mode is enabled.
		unsigned long mode = !blockMode;
		m_Error = ioctlsocket(m_Socket, FIONBIO, &mode);


		if (m_Error != NO_ERROR)
		{
			printf("ioctlsocket failed with error: %ld\n", m_Error);
			throw(Exception(GetError()));
		}
			


	}


	////////////////////////////////////////////////////DATA SOCKET

	DataSocket::DataSocket(sock socket)
		:Socket(socket),
		m_IsConnected(false)
	{
		if (socket != INVALID_SOCKET)
		{
			socklen_t s = sizeof(m_RemoteInfo);
			getpeername(socket, (sockaddr*)(&m_RemoteInfo), &s);
			m_IsConnected = true;
		}

	}

	void DataSocket::Connect(ipaddress p_addr, port p_port)
	{

		// if the socket is already connected...
		if (m_IsConnected == true)
		{
			printf("%s \n", "Socket already connected");
			throw Exception(EAlreadyConnected);
		}

		// first try to obtain a socket descriptor from the OS, if
		// there isn't already one.
		if (m_Socket == INVALID_SOCKET)
		{
			m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			// throw an exception if the socket could not be created
			if (m_Socket == INVALID_SOCKET) {
				wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
				throw Exception(GetError());
			}
		}

		// set up the socket address structure
		m_RemoteInfo.sin_family = AF_INET;
		m_RemoteInfo.sin_port = htons(p_port);
		inet_pton(AF_INET, INADDR_ANY, &(m_RemoteInfo.sin_addr));

		// now the socket is created, so connect it.
		socklen_t s = sizeof(struct sockaddr);
		m_Error = connect(m_Socket, (SOCKADDR*)&m_RemoteInfo, sizeof(sockaddr_in));
		if (m_Error == INVALID_SOCKET)
		{
			wprintf(L"connect failed with error: %d\n", WSAGetLastError());
			throw Exception(GetError());//after throw the socket should eb closed and wsa cleanup performed
			/*closesocket(m_Socket);
			WSACleanup();*/
		}

		m_IsConnected = true;

		// to get the local port, you need to do a little more work
		m_Error = getsockname(m_Socket, (struct sockaddr*)&m_LocalInfo, &s);
		if (m_Error != 0)
		{
			wprintf(L"getsockname failed with error: %d\n", WSAGetLastError());
			throw Exception(GetError());
			/*closesocket(m_Socket);
			WSACleanup();*/
		}

	}

	int DataSocket::Send(const char * p_buffer, int p_size)
	{
		// make sure the socket is connected first.
		if (m_IsConnected == false)
		{
			printf("%s \n", "Socket not connected for send");
			throw Exception(ENotConnected);
		}

		// attempt to send the data
		m_Error = send(m_Socket, p_buffer, p_size, 0);
		if (m_Error == SOCKET_ERROR)
		{
			wprintf(L"send failed with error: %d\n", WSAGetLastError());
			/*closesocket(m_Socket);
			WSACleanup();*/

			Error e = GetError();
			if (e != EOperationWouldBlock)
			{
				throw Exception(e);
			}
			// if the socket is nonblocking, we don't want to send a terminal
			// error, so just set the number of bytes sent to 0, assuming
			// that the client will be able to handle that.
			m_Error = 0;
		}

		// return the number of bytes successfully sent
		return m_Error;

	}

	int DataSocket::Receive(char * p_buffer, int p_size)
	{
		// make sure the socket is connected first.
		if (m_IsConnected == false)
		{
			printf("%s \n", "Socket not connected for recv");
			throw Exception(ENotConnected);
		}

		// attempt to recieve the data
		m_Error = recv(m_Socket, p_buffer, p_size, 0);
		if (m_Error == 0)
		{
			printf("%s \n", "Socket was closed");
			throw Exception(EConnectionClosed);
		}
		if (m_Error == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)//socket is set to nonblocking so just ignore it it wants to receive but nothing there to receive
			{
				//printf("%s \n", "Socket ignoring error WSAWOULDBLOCK 10035");
				return m_Error;
			}
			printf("recv failed: %d\n", WSAGetLastError());
			throw Exception(GetError());
		}

		// return the number of bytes successfully recieved
		return m_Error;

	}

	void DataSocket::Close()
	{
		if (m_IsConnected == true)
		{
			shutdown(m_Socket, SD_BOTH);
		}
		else
		{
			printf("%s \n", "Socket was closed already returning !!!!!!!!!");
			return;
		}
		// close the socket
		Socket::Close();

		m_IsConnected = false;
	}

	////////////////////////////////////////////////////LISTENING SOCKET


	ListeningSocket::ListeningSocket()
	{
		m_IsListening = true;
	}

	void ListeningSocket::Listen(port p_port, std::string ip_Address)
	{
		// first try to obtain a socket descriptor from the OS, if
		// there isn't already one.
		if (m_Socket == INVALID_SOCKET)
		{
			m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			// throw an exception if the socket could not be created
			if (m_Socket == INVALID_SOCKET)
			{
				wprintf(L"socket function failed with error: %ld\n", WSAGetLastError());
				throw Exception(GetError());
				/*WSACleanup();*/
			}
		}
		// set the SO_REUSEADDR option on the socket, so that it doesn't
		// hog the port after it closes.
		int reuse{ 1 };
		m_Error = setsockopt(m_Socket, SOL_SOCKET, SO_REUSEADDR,(char*)(&reuse), sizeof(reuse));
		if (m_Error == SOCKET_ERROR)
		{
			wprintf(L"setsockopt for SO_KEEPALIVE failed with error: %u\n", WSAGetLastError());
			throw Exception(GetError());
		}

		//// set up the socket address structure
		//m_LocalInfo.sin_family = AF_INET;
		//m_LocalInfo.sin_port = htons(p_port);
		//inet_pton(AF_INET, ip_Address.c_str(), &(m_LocalInfo.sin_addr));
		// set up the socket address structure

		//need to see if we can manually change the ip
		m_LocalInfo.sin_family = AF_INET;
		m_LocalInfo.sin_port = htons(p_port);
		m_LocalInfo.sin_addr.s_addr = htonl(INADDR_ANY);
		memset(&(m_LocalInfo.sin_zero), 0, 8);


		// bind the socket
		m_Error = bind(m_Socket, (sockaddr*)&m_LocalInfo, sizeof(sockaddr_in));
		if (m_Error == SOCKET_ERROR) {
			wprintf(L"bind function failed with error %d\n", WSAGetLastError());
			throw Exception(GetError());
			/*WSACleanup();*/
		}

		if (listen(m_Socket, SOMAXCONN) == SOCKET_ERROR)
		{
			wprintf(L"listen function failed with error: %d\n", WSAGetLastError());
			throw Exception(GetError());
		}
		m_IsListening = true;
	}

	

	DataSocket ListeningSocket::Accept()
	{
		sock s;
		struct sockaddr_in socketaddress;
		// try to accept a connection
		socklen_t size = sizeof(sockaddr_in);
		printf("%s \n", "Socket blocket for accept");
		s = accept(m_Socket, (sockaddr*)&socketaddress, &size);
		if (s == INVALID_SOCKET)
		{
			wprintf(L"socket accept function failed with error: %ld\n", WSAGetLastError());
			throw Exception(GetError());
		}

		printf("%s \n", "Socket Accepted and returning");
		// return the newly created socket.
		return DataSocket(s);
	}

	void ListeningSocket::Close()
	{
		// close the socket
		Socket::Close();
		// invalidate the variables
		m_IsListening = false;
	}

}