#pragma once
#include "Map.h"
#include "EnemyHandler.h"
#include "ItemHandler.h"
#include <chrono>
#include "HelperFunctions.h"

class GameLoop
{
public:
	using Ms = std::chrono::milliseconds;
	using Sec = std::chrono::seconds; //defined as long long if used with auto
	using sys_clock = std::chrono::system_clock;// represent wall clock time from the system-wide realtime clock, can be cast to timepoint
	using sd_clock = std::chrono::steady_clock;//That is, the clock may not be adjusted
	using hr_clock = std::chrono::high_resolution_clock; //represent clocks with the shortest tick period.(since start pc )
	using a_clock = hr_clock;//can be swapped easy for any you need
	using timePoint = std::chrono::time_point<a_clock>;
	using sint64 = long long;

	void initialize();
	void LoadDatabases();       // load all databases
	void SaveDatabases();       // save all databases
	void Loop();                // perform one loop iteration
	void performAutosave();
	void performEnemyAttack(); //performed every second enemy attack player
	void stockUpStores();
	void restoreMainGateKey(); 
	static long long getStartPoint();
	static void startTeacherTimer();
	
protected:
	sint64 m_savedatabases{0};
	sint64 m_nextround{0};
	sint64 m_nextregen{0};
	sint64 m_nextheal{0};
	sint64 m_teacherCheck{ 0 };
	bool m_lock{ false };
	
	static a_clock s_clock;
};  // end class GameLoop