#include "TrainHandler.h"
#include"BasicLibString.h"
#include"AllOnlinePlayers.h"

using namespace SocketLib;

namespace SimpleMUD
{

	// ------------------------------------------------------------------------
	//  This handles incomming commands. Anything passed into this function
	//  is assumed to be a complete command from a client.
	// ------------------------------------------------------------------------
	void Train::Handle(string p_data)
	{
		using namespace BasicLib;

		p_data = BasicLib::LowerCase(ParseWord(p_data, 0));

		Player& p = OnlinePlayers::getOnlinePlayer(m_playerID);

		if (p_data == "quit")
		{
			p.getConn()->RemoveHandler();
			p.getRoomRef().storePlayer(p.getID());
			return;
		}

		char n = p_data[0];
		if (n >= '1' && n <= '3')
		{
			if (p.getStatPoints() > 0)
			{
				p.subtractStatPoint();
				p.addTobaseAttr(n - '1', 1);
			}
		}

		PrintStats(true);
	}

	// ------------------------------------------------------------------------
	//  This notifies the handler that there is a new connection
	// ------------------------------------------------------------------------
	void Train::Enter()
	{
		Player& p = OnlinePlayers::getOnlinePlayer(m_playerID);

		if (p.getNewbie())
		{
			p.SendString(blue + bold +
				"Welcome to Monster Univesity, " + p.getEntName() + "!\r\n" +
				"You must train your character with your desired stats,\r\n" +
				"before you enter the realm.\r\n\r\n");
			p.setNewbie(false);
		}

		PrintStats(false);
	}

	// ------------------------------------------------------------------------
	//  This function prints out your statistics.
	// ------------------------------------------------------------------------
	void Train::PrintStats(bool p_clear)
	{
		using BasicLib::tostring;

		Player& p = OnlinePlayers::getOnlinePlayer(m_playerID);

		if (p_clear)
		{
			p.SendString(clearscreen);
		}


		p.SendString(white + bold +
			"--------------------------------- Your Stats ----------------------------------\r\n" +
			"Player:           " + p.getEntName() + "\r\n" +
			"Level:            " + tostring(p.getCurrLevel()) + "\r\n" +
			"Stat Points Left: " + tostring(p.getStatPoints()) + "\r\n" +
			"1) Strength:      " + tostring(p.getSTR()) + "\r\n" +
			"2) Health:        " + tostring(p.getMaxHealth()) + "\r\n" +
			"3) Agility:       " + tostring(p.getAGI()) + "\r\n" +
			bold +
			"-------------------------------------------------------------------------------\r\n" +
			"Enter 1, 2, or 3 to add a stat point, or \"quit\" to enter the realm: ");
	}


}   // end namespace SimpleMUD

