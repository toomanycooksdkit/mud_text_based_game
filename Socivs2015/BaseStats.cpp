#include "BaseStats.h"

void BaseStats::addAnotherbaseStats(BaseStats & another)
{
	m_Health.addAnotherHealth(another.m_Health);
	m_AGI += another.m_AGI;
	m_STR += another.m_STR;
}

void BaseStats::removeAnotherBaseStats(BaseStats & another)
{
	m_Health.removeAnotherHealth(another.m_Health);
	m_AGI -= another.m_AGI;
	m_STR -= another.m_STR;
}
