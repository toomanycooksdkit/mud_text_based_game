#pragma once
#include <iostream>
#include <string>
#include <type_traits> //for std::underlying_type

#include"Entity.h"
#include"BaseStats.h"
#include"Price.h"
#include"SocialType.h"
#include"ItemType.h"
#include "soci.h"

class Item
{
private:
	Entity m_entity;
public:
	BaseStats m_baseStats;
	Price m_price;
	SocialType m_socialBonus;
	ItemType m_itemType{ ItemType::Weapon };

	//CONSTRUCTORS
	Item() = default;
	Item(Entity entity, BaseStats baseStats, Price price, SocialType sType, ItemType itemType);

	template<typename String, typename Int, typename ItmType, typename Bool>
	Item(Int && p_entId, String && p_entName, Int && p_AGI, Int && p_STR, String && p_bstName,
		Int && p_curHealth, Int && p_maxHealth, Int && p_regenAmount, ItmType && p_iType,
		Bool && p_sellable, Int && p_buyPrice, Int && p_sellPrice) : m_entity(std::forward<String>(p_entName), std::forward<Int>(p_entId))
	{
		std::cout << "item forward constructor" << std::endl;
		m_baseStats.m_AGI = std::forward<Int>(p_AGI);
		m_baseStats.m_STR = std::forward<Int>(p_STR);
		m_baseStats.m_BSTName = std::forward<String>(p_bstName);
		m_baseStats.m_Health.m_currentHealth = std::forward<Int>(p_curHealth);
		m_baseStats.m_Health.m_maxHealth = std::forward<Int>(p_maxHealth);
		m_baseStats.m_Health.m_regenAmount = std::forward<Int>(p_regenAmount);
		m_itemType = std::forward<ItmType>(p_iType);
		m_price.sellable = std::forward<Bool>(p_sellable);
		m_price.m_buyPrice = std::forward<Int>(p_buyPrice);
		m_price.m_SellPrice = std::forward<Int>(p_sellPrice);
	}

	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

	void printItem() const;
};

namespace soci
{
	template<>
	struct type_conversion<Item>
	{
		typedef values base_type;

		static void from_base(values const & v, indicator /* ind */, Item & itm)
		{
			itm.setID(v.get<int>("entity_id"));
			itm.setEntName(v.get<std::string>("entity_name", "unknown"));
			itm.m_baseStats.m_AGI = v.get<int>("agi");
			itm.m_baseStats.m_STR = v.get<int>("str");
			itm.m_baseStats.m_BSTName = v.get<std::string>("base_name");
			itm.m_baseStats.m_Health.m_currentHealth = v.get<int>("curr_health");
			itm.m_baseStats.m_Health.m_maxHealth = v.get<int>("max_health");
			itm.m_baseStats.m_Health.m_regenAmount = v.get<int>("regen_amount");
			itm.m_itemType = static_cast<ItemType>(v.get<int>("item_type"));
			itm.m_socialBonus.m_socialPercentage = v.get<int>("social_percentage");
			itm.m_socialBonus.m_enemyType = static_cast<EnemyType>(v.get<int>("enemy_type"));
			itm.m_price.m_buyPrice = v.get<int>("buy_price");
			itm.m_price.m_SellPrice = v.get<int>("sell_price");
		
		}


		static void to_base(const Item & itm, values & v, indicator & ind)
		{
			v.set("entity_id", itm.getID());
			v.set("entity_name", itm.getEntName(), itm.getEntName().empty() ? i_null : i_ok);
			v.set("agi", itm.m_baseStats.m_AGI);
			v.set("str", itm.m_baseStats.m_STR);
			v.set("base_name", itm.m_baseStats.m_BSTName);
			v.set("curr_health", itm.m_baseStats.m_Health.m_currentHealth);
			v.set("max_health", itm.m_baseStats.m_Health.m_maxHealth);
			v.set("regen_amount", itm.m_baseStats.m_Health.m_regenAmount);
			int itmType = to_integral(itm.m_itemType);
			v.set("item_type", itmType);//can be replaced with v.set("item_type", to_integral(itm.m_itemType));
			v.set("social_percentage", itm.m_socialBonus.m_socialPercentage);
			int enemType = to_integral(itm.m_socialBonus.m_enemyType);
			v.set("enemy_type", enemType);
			v.set("buy_price", itm.m_price.m_buyPrice);
			v.set("sell_price", itm.m_price.m_SellPrice);
			ind = i_ok;
		}

	};
}