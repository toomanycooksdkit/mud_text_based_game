#include "Parser.h"
#include "Telnet.h"

bool Parser::parse(const std::string & toParse)
{
	bool toReturn{ false };

	for (auto& cmd : m_AllCommands)
	{
		toReturn = cmd.isCommandInSentence(toParse);
		if (toReturn == true)
		{
			break;
		}
	}
	return toReturn;
}

void Parser::parseAndExecute(const std::string & toParse, Player& plr)
{
	for (auto& cmd : m_AllCommands)
	{
		if (cmd.ifFoundExecute(toParse, plr))
		{
			break;//if we take this out it will execute all comand in one sentence by the user otherwise execute only one
		}
	}
}




void Parser::addComand(const Command & toAdd)
{
	m_AllCommands.push_back(toAdd);
}

void Parser::changeCommand(std::string cmdName, std::string shortcut)
{
	for (auto& cmd : m_AllCommands)
	{
		if (cmd.getCommandStr() == cmdName)
		{
			std::cout << "Commmand: " << cmdName << "shortcut set to: " << shortcut << std::endl;
			cmd.setShortcut(shortcut);
		}
	}
}

std::string Parser::getAllComandsDescription()
{
	std::string toRet{ "" };
	for (const auto& cmd : m_AllCommands)
	{
		toRet += "Comand: " + cmd.getCommandStr() + " | " + cmd.getShortcut() + "\t" + Lukas::green + cmd.getDescription() + "\r\n";
	}
	return toRet;
}
