#pragma once

#include"Player.h"
#include<vector>
#include<mutex>
#include<list>
#include "Parser.h"
#include "soci.h"
#include "soci-postgresql.h"
#include "SocketLibIncludes_Types.h"

class OnlinePlayers
{
private:

public:
	static Player & addPlayerWithReturnRef(Player p_toAdd);
	static void logoutPlayer(int pID, Parser & plrParser);
	static void autoSaveAllPlayers();
	static void updatePlayerInDB(Player plr, Parser & plrParser,soci::session & sql);
	static Player& getOnlinePlayer(int pID);
	static Player * getOnlinePlayerByName(const std::string & pName);
	static bool isEmpty();
	static void sendAllPlayers(std::string toSend);
	static std::string getAllOnlineNames();
	static std::list<Player>& getOnlinePlayersRef();
	static bool isConnected(Lukas::ipaddress toCheck);
	static bool isConnectedNumberOfTimes(Lukas::ipaddress toCheck, int howManyAllowedTimes);
};

