#pragma once

#include<vector>
#include"SocketLibLukasIncludes.h"
#include"ConnectionManager.h"

namespace Lukas
{
// Forward declarations
template<typename protocol, typename defaulthandler>
class ConnectionManager;


template<typename protocol, typename defaulthandler>
class ListeningManager
{
private:
	//std::vector<ListeningSocket> m_ListenSocks;

	ListeningSocket m_ListeningSock;

	ConnectionManager<protocol, defaulthandler>* m_Manager; //can be unique pointer

public:


	void StartOnPort5099();

	void SetConnectionManager(ConnectionManager<protocol, defaulthandler>* p_manager);

	void Listen();


};

template<typename protocol, typename defaulthandler>
inline void ListeningManager<protocol, defaulthandler>::StartOnPort5099()
{
	//// create a new socket
	//ListeningSocket lsock;
	// listen on the requested port
	m_ListeningSock.Listen(5099,"192.168.1.6");

	

}

template<typename protocol, typename defaulthandler>
inline void ListeningManager<protocol, defaulthandler>::SetConnectionManager(ConnectionManager<protocol, defaulthandler>* p_manager)
{
	m_Manager = p_manager;
}

template<typename protocol, typename defaulthandler>
inline void ListeningManager<protocol, defaulthandler>::Listen()
{
	// define a data socket that will receive connections from the listening
	// sockets
	DataSocket datasock;

	//m_ListeningSock.SetBlocking(false);

	datasock = m_ListeningSock.Accept();


	//create thread and pass the datasocket into it <<
	m_Manager->NewConnection(datasock);

}
















}