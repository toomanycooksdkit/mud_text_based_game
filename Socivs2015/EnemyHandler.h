#pragma once

#include <vector>
#include <algorithm>
#include <random>

#include "Enemy.h"
#include"HelperFunctions.h"
#include "soci.h"
#include "soci-postgresql.h"
#include<list>

//std::random_device m_Rd; https://msdn.microsoft.com/en-us/library/bb982398.aspx
//std::uniform_int_distribution<int> dist();
//std::mt19937 m_Mt;


//could be std::array if know size at advance //http://stackoverflow.com/questions/4424579/stdvector-versus-stdarray-in-c


class EnemyHandler
{
private:

public:
	static void loadEnemies();
	static void saveEnemies();
	static void add(Enemy& toAdd);
	static std::vector<Enemy> generateRoomEnemys(int number);
	static std::list<Enemy> generateRoomEnemysList(int number);
	static std::vector<Enemy> generateRoomEnemysSeedet(int number, int seed);
	static void shuffleVector();
	static void printEnemies();
};

