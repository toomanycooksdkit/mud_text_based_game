#include "Command.h"

Command::Command(std::string cmd, std::string sh, std::function<void(Player&)> fToExecute)
	: m_Command(cmd),m_Shortcut(sh),m_ExecuteCommand(fToExecute)
{

}

void Command::executeCommand(Player& pToSend)
{
	m_ExecuteCommand(pToSend);
}

bool Command::isCommandInSentence(const std::string toParse)
{
	std::string REGEX_GET = R"(\b()" + m_Command + R"(|)" + m_Shortcut + R"()\b)";
	std::cout << REGEX_GET << std::endl;
	std::cout << toParse << std::endl;
	std::regex rx(REGEX_GET);
	std::smatch pieces_match;
	if (std::regex_search(toParse, pieces_match, rx))
	{
		return true;
	}

	return false;//not found in the sentence
}

bool Command::ifFoundExecute(const std::string toParse, Player & plr)
{

	std::string REGEX_GET = R"(\b()" + m_Command + R"(|)" + m_Shortcut + R"()\b)";
	std::regex rx(REGEX_GET);
	std::smatch pieces_match;
	if (std::regex_search(toParse, pieces_match, rx))
	{
		//stop the distraction here because he is executing another command
		if (plr.m_isDistracting == true)
		{
			plr.distractionStopped();
		}
		m_ExecuteCommand(plr);
		return true;
	}
	return false;
}