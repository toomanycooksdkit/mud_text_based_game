#include "HelperFunctions.h"

//Fast ?
//Crypto - secure ?
//Seedable ?
//Deterministic ?

int getRandomIntWithSeed(int seed, int min, int max)
{
	std::mt19937 mt(seed);
	std::uniform_int_distribution<int> dist(min, max);

	return dist(mt);
}

int getRandomMt19937(int min, int max)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(min, max);

	return dist(mt);
}

int getRandomCryptoSecure(int min, int max)
{
	std::random_device rd;
	std::uniform_int_distribution<int> dist(min, max);
	return dist(rd);
}

