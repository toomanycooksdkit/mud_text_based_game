#include "Room.h"
#include "AllOnlinePlayers.h"
#include "BasicLibString.h"
#include "Telnet.h"

constexpr int MAXITEMS { 10 };

bool Room::removeEnemyByType(const EnemyType & type)
{
	int counter{ 0 };
	m_enemies.remove_if([type, & counter](Enemy e) 
	{ 
		if (e.m_type == type)
		{
			std::cout << "removing " << std::endl;
			counter++;
			return true;
		}
		return false;
	});

	if (counter == 0)
	{
		//nothing was removed
		return false;
	}

	return true;
}

void Room::printRoom() const
{
	m_entity.printEntity();
	for (auto & enem : m_enemies)
	{
		enem.printEnemy();
	}
}

std::string Room::getPlayerNamesInRoom()
{
	std::string toRet{ Lukas::magenta + "PLAYERS: "};
	for (auto & plr : m_playersInRoom)
	{
		toRet += OnlinePlayers::getOnlinePlayer(plr).getEntName() + "  ";
	}
	return toRet;
}

void Room::storePlayer(int plr)// Player plr)
{
	m_playersInRoom.emplace_back(plr);
}

void Room::removePlayer(int plr)// Player plr)
{
	if (m_playersInRoom.empty())
	{
		std::cout << "There's no one here." << std::endl;
	}
	else
	{
		for (auto it = m_playersInRoom.begin(); it != m_playersInRoom.end(); it++)
		{
			if (*it == plr)
			{
				std::cout << "Leaving the room." << std::endl;
				m_playersInRoom.erase(it);
				
				return;
			}

		}

	}
}

void Room::sendAllInRoom(const std::string & data)
{
	std::cout << "Sending to room" << std::endl;
	for (auto & plr : m_playersInRoom)
	{
		OnlinePlayers::getOnlinePlayer(plr).SendString(data);
	}
}

void Room::addNPC(NPC toAdd)
{
	m_NPCsInRoom.emplace_back(toAdd);
}

void Room::addItem(Item toAdd)
{
	m_items.emplace_back(toAdd);
}

NPC * Room::getNPCptr(int id)
{
	for (auto & npc : m_NPCsInRoom)
	{
		if (npc.m_entity.m_ID == id)
		{
			 return &npc;
		}
	}
	return nullptr;
}

NPC Room::removeNPCbyID(int id)
{
	NPC toRet;
	for (auto & npc : m_NPCsInRoom)
	{
		if (npc.m_entity.m_ID == id)
		{
			toRet = npc;
			m_NPCsInRoom.pop_back();
			return toRet;
		}
	}
	return toRet;

}


void Room::ifEmptyGenerateEnemies()
{
	
	if (m_enemies.empty() && m_roomType == RoomType::Standard )
	{
		std::cout << "generating enemies" << std::endl;
		int numToGenerate{ 3 };
		m_enemies =	EnemyHandler::generateRoomEnemysList(numToGenerate);
	}
	
}

void Room::getItemsOnFloorAfterKill(std::list<Item> itemsAfterKill)
{
	if (m_items.size() < MAXITEMS)
	{
		m_items.splice(m_items.begin(), itemsAfterKill);//splice the lists if there are any items on the floor
	}
}

void Room::getMoneyOnFloorAfterKill(int money)
{
	m_money += money;//add the money to the pile
}

Item Room::removeFirstPickedItem()
{
	Item toRet;
	if (!m_items.empty())
	{
		toRet = m_items.front();
		m_items.pop_front();
		return toRet;
	}
	return toRet;//empty item
}

Item Room::removeFirstBoughtItem(Player & plr)
{
	Item toRet;
	if (!m_items.empty())
	{
		if (plr.canBuy(m_items.front().m_price.m_buyPrice))
		{
			toRet = m_items.front();
			m_items.pop_front();
			return toRet;
		}
		else
		{
			return toRet;
		}
	}
	return toRet;//empty item
}

Item Room::removeItemByName(std::string toRemove)
{
	Item toRet;//initialized empty
	if (!m_items.empty())
	{
		for (auto it = m_items.begin(); it != m_items.end(); it++)
		{
			if (BasicLib::LowerCase(it->getEntName()) == BasicLib::LowerCase(toRemove))
			{
				toRet = *it;
				std::cout << "erasing " << std::endl;
				m_items.erase(it);
				return toRet;
			}

		}
	}
	return toRet;//empty item if didnt find one in the room
}

Item Room::removeBoughtItemByName(std::string toRemove, Player & plr)
{
	Item toRet;//initialized empty
	if (!m_items.empty())
	{
		for (auto it = m_items.begin(); it != m_items.end(); it++)
		{
			
			if (BasicLib::LowerCase(it->getEntName()) == BasicLib::LowerCase(toRemove))
			{
				if (plr.canBuy(m_items.front().m_price.m_buyPrice))
				{
					toRet = *it;
					std::cout << "erasing " << std::endl;
					m_items.erase(it);
					return toRet;
				}
				else
				{
					//plr.SendString("You dont have enough money");
					return toRet;
				}
			}
		}
	}
	return toRet;//empty item if didnt find one in the room
}

void Room::attackPlayerByName(std::string attPlr, Player plr)
{
	if (!plr.getPvp())
	{
		plr.SendString("I thought you didn't want to fight? Change your PvP stance if you want to fight.");
		return;
	}

	int ref;
	bool ifHit = false;

	for (auto it = m_playersInRoom.begin(); it != m_playersInRoom.end(); it++)
	{
		if (m_playersInRoom.size() <= 1)
		{
			ifHit = true;
			plr.SendString("There's no one here to fight.");
		}
		else if (BasicLib::LowerCase(OnlinePlayers::getOnlinePlayer(*it).getEntName()) == BasicLib::LowerCase(attPlr))
		{
			ifHit = true;
			ref = *it;
			if (OnlinePlayers::getOnlinePlayer(ref).getPvp())
			{
				if (OnlinePlayers::getOnlinePlayer(ref).fought(plr))
				{	
					if (OnlinePlayers::getOnlinePlayer(ref).checkIfDead())
					{
						OnlinePlayers::sendAllPlayers(Lukas::red + OnlinePlayers::getOnlinePlayer(ref).getEntName() + " just got rekt by " + plr.getEntName());
						return;
					}
				}
			}
			else
			{
				plr.SendString(OnlinePlayers::getOnlinePlayer(*it).getEntName() + " does not want to fight.");
			}
		}
	}

	if (!ifHit)
	{
		plr.SendString("There is no one here with that name.");
	}
}

void Room::whisperToPlayerByName(std::string whisperPlr, std::string chat, Player plr)
{
	for (auto it = m_playersInRoom.begin(); it != m_playersInRoom.end(); it++)
	{
		if (BasicLib::LowerCase(OnlinePlayers::getOnlinePlayer(*it).getEntName()) == BasicLib::LowerCase(whisperPlr))
		{
			OnlinePlayers::getOnlinePlayer(*it).SendString(Lukas::bwhite + Lukas::magenta + plr.getEntName() + ": " + BasicLib::RemoveWord(BasicLib::RemoveWord(chat, 0), 0) + Lukas::reset);
		}
	}
}

void Room::enemyAttacks()
{
	for (auto & enem : m_enemies)
	{
		for (auto & plr : m_playersInRoom)
		{
			if (!m_playersInRoom.empty())
			{
				if (enem.attack(OnlinePlayers::getOnlinePlayer(plr)))
				{
					std::cout << "Player died during attack" << std::endl;
					break;
				}
				else
				{
					std::cout << "No one died during this attack" << std::endl;
				}
			}
		}
	}
}



