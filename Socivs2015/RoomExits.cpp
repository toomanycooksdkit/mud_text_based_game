#include "RoomExits.h"

std::string RoomExits::availableExits()
{
	std::string temp{""};
	if (m_north != 0)
	{
		temp += "North\r\n";
	}
	if (m_east != 0)
	{
		temp += "East\r\n";
	}
	if (m_south != 0)
	{
		temp += "South\r\n";
	}
	if (m_west != 0)
	{
		temp += "West\r\n";
	}
	return temp;
}
