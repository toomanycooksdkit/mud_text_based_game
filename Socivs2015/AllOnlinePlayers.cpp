#include "AllOnlinePlayers.h"
#include <algorithm>
#include "Shortcuts.h"

using namespace soci;
std::list<Player> m_onlinePlayers;
std::mutex m_VecMut;

Player & OnlinePlayers::addPlayerWithReturnRef(Player p_toAdd)
{
	std::lock_guard<std::mutex> lg(m_VecMut);
	m_onlinePlayers.push_back(p_toAdd);
	std::cout << "The ID for player returning: " << m_onlinePlayers.back().getID() << std::endl;
	return m_onlinePlayers.back();
}

void OnlinePlayers::logoutPlayer(int pID, Parser& plrParser)
{
	std::cout << "Logging out the player with ID and updating DB: " << pID << std::endl;
	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	if (m_onlinePlayers.empty())
	{
		std::cout << "No more players to log out " << std::endl;
	}
	else
	{
		for (auto it = m_onlinePlayers.begin(); it != m_onlinePlayers.end(); it++)
			{
				if (it->getID() == pID)
				{
					updatePlayerInDB(*it, plrParser,sql);
					std::cout << "erasing " << std::endl;
					m_onlinePlayers.erase(it);
					return;
				}
			}
	}	
}

void OnlinePlayers::autoSaveAllPlayers()
{
	session sql(postgresql, "dbname=GameDB user=postgres password=password");
	for (auto & plr : m_onlinePlayers)
	{
		updatePlayerInDB(plr, plr.getParser(), sql);
	}
}

void OnlinePlayers::updatePlayerInDB(Player plr, Parser& plrParser, session & sql)
{
	
	transaction tr(sql);
	std::cout << "updating player in DATABASE: " << plr.getEntName() << std::endl;
	//update player before logging him out
	sql << "update players"
		" set bs_name = :bs_name, plr_password = :plr_password, str = :str, agi = :agi, curr_health = :curr_health, regen_amount = :regen_amount, max_health = :max_health, rank = :rank, curr_exp = :curr_exp, curr_level = :curr_level, max_level = :max_level, next_level_xp = :next_level_xp, money = :money, room_id = :room_id, att_speed = :att_speed, stat_points = :stat_points"
		" where entity_id = :entity_id",
		use(plr);
	//update inventory when logging out
	sql << "update inventory"
		" set pos0 = :pos0, pos1 = :pos1, pos2 = :pos2, pos3 = :pos3, pos4 = :pos4, pos5 = :pos5, pos6 = :pos6, pos7 = :pos7, pos8 = :pos8, pos9 = :pos9 "
		" where entity_id = :entity_id",
		use(plr.getID(), "entity_id"), use(plr.getInventory());
	//get the changed commands into shortcuts structure and save into database
	Shortcuts shc;
	shc.setID(plr.getID());//important to set that the shortcuts update only the correct player and not all
	for (auto & cmd : plrParser.allComandsRef())
	{
		shc.m_shortcuts.emplace_back(cmd.getShortcut());
	}
		sql << "update shortcuts"
	" set change = :change, stat = :stat, chat = :chat, shut = :shut, north = :north, south = :south, east = :east, west = :west, whisper = :whisper, attack = :attack, pick = :pick, use = :use, help = :help, who = :who, whoall = :whoall, promote = :promote, demote = :demote, look = :look, get = :get, distract = :distract "
	" where entity_id = :entity_id",
	use(shc);

	tr.commit();
}


Player & OnlinePlayers::getOnlinePlayer(int pID)
{
	for (auto it = m_onlinePlayers.begin(); it != m_onlinePlayers.end(); it++)
	{
		if (it->getID() == pID)
		{
			return *it;
		}

	}
	Player p;//if player dosn exists
	std::cout << "player does not exists returning empty player" << std::endl;
	return p;
}

Player * OnlinePlayers::getOnlinePlayerByName(const std::string & pName)
{
	for (auto it = m_onlinePlayers.begin(); it != m_onlinePlayers.end(); it++)
	{
		if (it->getEntName() == pName)
		{
			return &(*it);
		}
	}
	
	return nullptr;
}

bool OnlinePlayers::isEmpty()
{
	std::lock_guard<std::mutex> lg(m_VecMut);
	return m_onlinePlayers.empty();
}

void OnlinePlayers::sendAllPlayers(std::string toSend)
{
	std::lock_guard<std::mutex> lg(m_VecMut);
	if (!m_onlinePlayers.empty())
	{
		for (auto player : m_onlinePlayers)
		{
			player.SendString(toSend);
		}
	}
}

std::string OnlinePlayers::getAllOnlineNames()
{
	std::string temp{ "ONLINE PLAYERS\r\n" };
	for (auto& player : m_onlinePlayers)
	{
		temp += player.getEntName() + "\r\n";
	}
	return temp;
}

std::list<Player>& OnlinePlayers::getOnlinePlayersRef()
{
	return m_onlinePlayers;
}

bool OnlinePlayers::isConnected(Lukas::ipaddress toCheck)
{
	for (auto& player : m_onlinePlayers)
	{
		if (player.getConn()->GetRemoteAddress() == toCheck)
		{
			std::cout << "IP connected " << toCheck << std::endl;
			return true;
		}
	}

	return false;
}

bool OnlinePlayers::isConnectedNumberOfTimes(Lukas::ipaddress toCheck, int howManyAllowedTimes)
{
	int count{ 0 };
	for (auto& player : m_onlinePlayers)
	{
		if (player.getConn()->GetRemoteAddress() == toCheck)
		{
			count++;
		}
	}

	if (count >= howManyAllowedTimes)
	{
		std::cout << "IP connected more than" << howManyAllowedTimes << "times" << std::endl;
		return true;
	}

	return false;
}
