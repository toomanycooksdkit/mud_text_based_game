#pragma once
#include<vector>
#include<string>
#include"Entity.h"
#include "soci.h"

class Shortcuts
{
private:
	Entity m_entity;
public:

	std::vector<std::string> m_shortcuts;
	
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

};

namespace soci
{
	template<>
	struct type_conversion<Shortcuts>
	{
		typedef values base_type;

		static void from_base(values const & v, indicator /* ind */, Shortcuts & shc)
		{
			//itm.m_entity.m_ID = v.get<int>("entity_id");
			shc.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			shc.setEntName(v.get<std::string>("entity_name", "unknown"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("change"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("stat"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("chat"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("shut"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("north"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("south"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("east"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("west"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("whisper"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("attack"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("pick"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("use"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("help"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("who"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("whoall"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("promote"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("demote"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("look"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("get"));
			shc.m_shortcuts.emplace_back(v.get<std::string>("distract"));
		}


		static void to_base(const Shortcuts & shc, values & v, indicator & ind)
		{
			v.set("entity_id", shc.getID());
			//v.set("entity_name", itm.m_entity.m_name, itm.m_entity.m_name.empty() ? i_null : i_ok);
			//std::string entName = itm.getEntName();
			v.set("entity_name", shc.getEntName(), shc.getEntName().empty() ? i_null : i_ok);
			//std::string sh = shc.m_shortcuts.at(0);
			v.set("change", shc.m_shortcuts[0]);
			v.set("stat", shc.m_shortcuts[1]);
			v.set("chat", shc.m_shortcuts[2]);
			v.set("shut", shc.m_shortcuts[3]);
			v.set("north", shc.m_shortcuts[4]);
			v.set("south", shc.m_shortcuts[5]);
			v.set("east", shc.m_shortcuts[6]);
			v.set("west", shc.m_shortcuts[7]);
			v.set("whisper", shc.m_shortcuts[8]);
			v.set("attack", shc.m_shortcuts[9]);
			v.set("pick", shc.m_shortcuts[10]);
			v.set("use", shc.m_shortcuts[11]);
			v.set("help", shc.m_shortcuts[12]);
			v.set("who", shc.m_shortcuts[13]);
			v.set("whoall", shc.m_shortcuts[14]);
			v.set("promote", shc.m_shortcuts[15]);
			v.set("demote", shc.m_shortcuts[16]);
			v.set("look", shc.m_shortcuts[17]);
			v.set("get", shc.m_shortcuts[18]);
			v.set("distract", shc.m_shortcuts[19]);
			ind = i_ok;
		}

	};
}