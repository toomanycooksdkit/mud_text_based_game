#pragma once

struct Price
{
	int m_buyPrice{ 0 };
	int m_SellPrice{ 0 };
	bool sellable{ false };
};