#pragma once

#include"Item.h"
#include"PlayerTypes.h"
#include"Level.h"
#include"Inventory.h"
#include"Room.h"

class Player
{
private:
	Entity m_entity;
	std::string m_password;
	BaseStats m_baseStats;
	PlayerRank m_rank{ PlayerRank::Regular };
	Level m_level;
	Inventory m_inventory;
	int m_money{ 0 };
	//Social type
	std::shared_ptr<Room> m_room_ptr;
	int m_roomID{ 0 };
	int m_speed{ 0 };
	int m_statPoints{ 0 };

public:
	
	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };
	inline std::string getPassword() const { return m_password; };
	inline void setPassword(const std::string& toSet) { m_password = toSet; };


	inline int getSTR()const { return m_baseStats.m_STR; };
	inline void setSTR(int str) { m_baseStats.m_STR = str; };
	inline int getAGI()const { return m_baseStats.m_AGI; };
	inline void setAGI(int agi) { m_baseStats.m_AGI = agi; };
	inline std::string getBsName() const { return m_baseStats.m_BSTName; };
	inline void setBsName(const std::string& toSet) { m_baseStats.m_BSTName = toSet; };
	inline int getCurrHealth()const { return m_baseStats.m_Health.m_currentHealth; };
	inline void setCurrHealth(int toSet) { m_baseStats.m_Health.m_currentHealth = toSet; };
	inline int getRegenAmount()const { return m_baseStats.m_Health.m_regenAmount; };
	inline void setRegenAmount(int toSet) { m_baseStats.m_Health.m_regenAmount = toSet; };
	inline int getMaxHealth()const { return m_baseStats.m_Health.m_maxHealth; };
	inline void setMaxHealth(int toSet) { m_baseStats.m_Health.m_maxHealth = toSet; };
	inline PlayerRank getRank()const { return m_rank; };
	inline void setRank(PlayerRank toSet) { m_rank = toSet; };
	
	inline int getCurrExp()const { return m_level.m_experienceBar; };
	inline void setCurrExp(int toSet) { m_level.m_experienceBar = toSet; };
	inline int getCurrLevel()const { return m_level.m_curLevel; };
	inline void setCurrLevel(int toSet) { m_level.m_curLevel = toSet; };
	inline int getMaxLevel()const { return m_level.m_maxLevel; };
	inline void setMaxLevel(int toSet) { m_level.m_maxLevel = toSet; };
	inline int getNextLevel()const { return m_level.m_nextLevelXP; };
	inline void setNextLevel(int toSet) { m_level.m_nextLevelXP = toSet; };

	inline int getMoney()const { return m_money; };
	inline void setMoney(int toSet) { m_money = toSet; };

	inline int getRoomID()const { return m_roomID; };
	inline void setRoomID(int toSet) { m_roomID = toSet; };

	inline int getSpeed()const { return m_roomID; };
	inline void setSpeed(int toSet) { m_roomID = toSet; };


	inline Inventory& getInventory() { return m_inventory; }
	inline int getStatPoints()const { return m_statPoints; };
	inline void setStatPoints(int toSet) { m_statPoints = toSet; };


	void printPlayer() const;
};




namespace soci
{
	template<>
	struct type_conversion<Player>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Player& plr)
		{
			//itm.m_entity.m_ID = v.get<int>("entity_id");
			plr.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			plr.setEntName(v.get<std::string>("entity_name", "unknown"));
			plr.setPassword(v.get<std::string>("plr_password"));
			plr.setSTR(v.get<int>("str"));
			plr.setAGI(v.get<int>("agi"));
			plr.setBsName(v.get<std::string>("bs_name", "unknown"));
			plr.setCurrHealth(v.get<int>("curr_health"));
			plr.setRegenAmount(v.get<int>("regen_amount"));
			plr.setMaxHealth(v.get<int>("max_health"));
			plr.setRank(static_cast<PlayerRank>(v.get<int>("rank")));
			plr.setCurrExp(v.get<int>("curr_exp"));
			plr.setCurrLevel(v.get<int>("curr_level"));
			plr.setMaxLevel(v.get<int>("max_level"));
			plr.setNextLevel(v.get<int>("next_level_xp"));
			plr.setMoney(v.get<int>("money"));
			plr.setRoomID(v.get<int>("room_id"));
			plr.setSpeed(v.get<int>("att_speed"));
			plr.setStatPoints(v.get<int>("stat_points"));
			
		}


		static void to_base(const Player& plr, values& v, indicator& ind)
		{
			v.set("entity_id", plr.getID());
			v.set("entity_name", plr.getEntName(), plr.getEntName().empty() ? i_null : i_ok);
			v.set("plr_password", plr.getPassword());
			v.set("str", plr.getSTR());
			v.set("agi", plr.getAGI());
			v.set("bs_name", plr.getBsName());
			v.set("curr_health", plr.getCurrHealth());
			v.set("regen_amount", plr.getRegenAmount());
			v.set("max_health", plr.getMaxHealth());
			int p_rank = to_integral(plr.getRank());
			v.set("rank", p_rank);
			v.set("curr_exp", plr.getCurrExp());
			v.set("curr_level", plr.getCurrLevel());
			v.set("max_level", plr.getMaxLevel());
			v.set("next_level_xp", plr.getNextLevel());
			v.set("money", plr.getMoney());
			v.set("room_id", plr.getRoomID());
			v.set("att_speed", plr.getSpeed());
			v.set("stat_points", plr.getStatPoints());
			//v.set("inventory", plr.getInventory());
			ind = i_ok;
		}

	};
}