#pragma once

#include"Item.h"
#include<array>

class Inventory
{
private:


public:
	/*std::vector<Item> m_items;
	int m_maxCapacity;*/
	Entity m_entity;
	std::array<Item, 10> m_items;

	bool haveSpace();

	bool addItem(Item toAdd);

};


namespace soci
{
	template<>
	struct type_conversion<Inventory>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Inventory& inv)
		{
			//itm.m_entity.m_ID = v.get<int>("entity_id");
			//add items from all items base of id
			std::cout << "getting positions" << std::endl;
			inv.m_items[0].setID(v.get<int>("pos0"));
			std::cout << inv.m_items[0].getID() << std::endl;
			inv.m_items[1].setID(v.get<int>("pos1"));
			inv.m_items[2].setID(v.get<int>("pos2"));
			inv.m_items[3].setID(v.get<int>("pos3"));
			inv.m_items[4].setID(v.get<int>("pos4"));
			inv.m_items[5].setID(v.get<int>("pos5"));
			inv.m_items[6].setID(v.get<int>("pos6"));
			inv.m_items[7].setID(v.get<int>("pos7"));
			inv.m_items[8].setID(v.get<int>("pos8"));
			inv.m_items[9].setID(v.get<int>("pos9"));

			
		}


		static void to_base(const Inventory& inv, values& v, indicator& ind)
		{
			/*v.set("entity_id", inv.m_entity.m_ID);
			v.set("entity_name", inv.m_entity.m_name);*/
			v.set("pos0", inv.m_items[0].getID());
			v.set("pos1", inv.m_items[1].getID());
			v.set("pos2", inv.m_items[2].getID());
			v.set("pos3", inv.m_items[3].getID());
			v.set("pos4", inv.m_items[4].getID());
			v.set("pos5", inv.m_items[5].getID());
			v.set("pos6", inv.m_items[6].getID());
			v.set("pos7", inv.m_items[7].getID());
			v.set("pos8", inv.m_items[8].getID());
			v.set("pos9", inv.m_items[9].getID());
			ind = i_ok;
		}

	};
}