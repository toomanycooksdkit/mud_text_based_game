#pragma once

#include<string>
#include<iostream>

struct Entity
{
	std::string m_name{ "emptyEntityName" };
	int m_ID{ 0 };

	Entity() = default;

	template<typename String, typename Int>
	Entity(String && p_name, Int && p_id) : m_name(std::forward<String>(p_name)), m_ID(std::forward<Int>(p_id)) { std::cout << " entity move constructor" << std::endl; }

	void printEntity() const;
};