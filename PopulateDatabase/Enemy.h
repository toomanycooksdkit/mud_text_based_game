#pragma once
#include <iostream>
#include <string>
#include <type_traits> //for std::underlying_type

#include "Entity.h"
#include "BaseStats.h"
#include "EnemyType.h"
#include "soci.h"
#include "ItemType.h"


class Enemy
{
private:
	Entity m_entity;
	//pointer to drop handler

public:
	BaseStats m_enemyStats;
	int m_socialSkill{ 0 };
	EnemyType m_type{ EnemyType::Troll };

	Enemy() = default;
	Enemy(Entity entity, BaseStats baseStats, int socialSkill, EnemyType type);

	//Getters & Setters
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

};

namespace soci
{
	template<>
	struct type_conversion<Enemy>
	{
		typedef values base_type;

		static void from_base(values const & v, indicator /* ind */, Enemy& enem)
		{


			//itm.m_entity.m_ID = v.get<int>("entity_id");
			enem.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			enem.setEntName(v.get<std::string>("entity_name", "unknown"));

			enem.m_enemyStats.m_AGI = v.get<int>("agi");
			enem.m_enemyStats.m_STR = v.get<int>("str");
			enem.m_enemyStats.m_BSTName = v.get<std::string>("base_name");

			enem.m_enemyStats.m_Health.m_currentHealth = v.get<int>("curr_health");
			enem.m_enemyStats.m_Health.m_maxHealth = v.get<int>("max_health");
			enem.m_enemyStats.m_Health.m_regenAmount = v.get<int>("regen_amount");

			enem.m_type = static_cast<EnemyType>(v.get<int>("enemy_type"));

			enem.m_socialSkill = v.get<int>("social_skill");
		}


		static void to_base(const Enemy& enem, values& v, indicator & ind)
		{
			v.set("entity_id", enem.getID());
			v.set("entity_name", enem.getEntName(), enem.getEntName().empty() ? i_null : i_ok);
			v.set("agi", enem.m_enemyStats.m_AGI);
			v.set("str", enem.m_enemyStats.m_STR);
			v.set("base_name", enem.m_enemyStats.m_BSTName);
			v.set("curr_health", enem.m_enemyStats.m_Health.m_currentHealth);
			v.set("max_health", enem.m_enemyStats.m_Health.m_maxHealth);
			v.set("regen_amount", enem.m_enemyStats.m_Health.m_regenAmount);
			int enemType = to_integral(enem.m_type);
			v.set("enemy_type", enemType);//can be replaced with v.set("item_type", to_integral(itm.m_itemType));
			v.set("social_skill", enem.m_socialSkill);
			ind = i_ok;
		}

	};
}

