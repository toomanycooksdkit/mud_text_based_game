#pragma once

#include<string>
#include"Health.h"

struct BaseStats
{
	std::string m_BSTName{ "emptyBSTName" };
	int m_STR{ 0 };
	int m_AGI{ 0 };
	Health m_Health;

};