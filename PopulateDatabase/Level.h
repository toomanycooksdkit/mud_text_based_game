#pragma once

struct Level
{
	int m_experienceBar{ 0 };
	int m_curLevel{ 0 };
	int m_maxLevel{ 0 };
	int m_nextLevelXP{ 0 };
};