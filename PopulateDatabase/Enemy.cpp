#include "Enemy.h"

Enemy::Enemy(Entity entity, BaseStats baseStats, int socialSkill, EnemyType type)
	:m_entity(entity), m_enemyStats(baseStats), m_socialSkill(socialSkill), m_type(type)
{
}
