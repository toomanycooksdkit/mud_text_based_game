#include "soci.h"
#include "soci-postgresql.h"
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <exception>
#include "PersonTest.h"
#include "Item.h"
#include "Room.h"
#include "Player.h"
#include "Enemy.h"
#include "Shortcuts.h"
#include<chrono>

#include<vector>
//this is version for VS2015
using namespace soci;
using namespace std;

void createTablesForDB(session& sql);
void dropTablesFromDB(session& sql);

void populateAllTables(session& sql);

void populatePlayers(session &sql);
std::vector<Player> getPlayersFromDatabase(session &sql);
void populateItems(session & sql);
std::vector<Item> getItemsFromDatabaseTable(session &sql, int howMany);
std::vector<Item> getItemsFromDatabaseTable(session &sql);
void populateRooms(session& sql);
std::vector<Room> getRoomsFromDatabaseTable(session& sql);
void populateEnemies(session& sql);
std::vector<Enemy> getEnemiesFromDatabaseTable(session& sql);
void populateShortcuts(session & sql);

void speedtestInserts(session & sql);

int main()
{
	try
	{
		//can create new users remember dbname user and password required
		session sql(postgresql, "dbname=GameDB user=postgres password=password");

		//Item i2(1, "Sword", 100, 20, "Sword", 100, 100, 0, ItemType::Weapon, true, 100, 70);
		//speedtestInserts(sql);

		dropTablesFromDB(sql);
		createTablesForDB(sql);
		populateAllTables(sql);
	}
	catch (soci::postgresql_soci_error const & e)
	{
		cerr << "PostgreSQL error: " << e.sqlstate()
			<< " " << e.what() << endl;
	}
	catch (exception const &e)
	{
		cerr << "Error: " << e.what() << '\n';
	}

	system("PAUSE");
}

void createTablesForDB(session & sql)
{
	sql << "CREATE TABLE bannedplayers(name varchar(20), days int , PRIMARY KEY (name) )";
	sql << "CREATE TABLE inventory(entity_id int NOT NULL, entity_name varchar(20), pos0 int, pos1 int, pos2 int, pos3 int, pos4 int, pos5 int, pos6 int, pos7 int, pos8 int, pos9 int )";
	sql << "CREATE TABLE shortcuts(entity_id int NOT NULL, entity_name varchar(20), change varchar(10), stat varchar(10), chat varchar(10), shut varchar(10), north varchar(10), south varchar(10), east varchar(10), west varchar(10), whisper varchar(10), attack varchar(10), pick varchar(10), use varchar(10), help varchar(10), who varchar(10), whoall varchar(10), promote varchar(10), demote varchar(10), look varchar(10), get varchar(10), distract varchar(10), PRIMARY KEY (entity_name))";
	sql << "CREATE TABLE rooms(entity_id int NOT NULL, entity_name varchar(20), east int, north int, south int, west int, description varchar(200), room_type int, PRIMARY KEY (entity_id))";
	sql << "CREATE TABLE players(entity_id int NOT NULL, entity_name varchar(20), plr_password varchar(64) NOT NULL, str int, agi int, bs_name varchar(20), curr_health int, regen_amount int, max_health int, rank int, curr_exp int, curr_level int, max_level int, next_level_xp int, money int, room_id int, att_speed int, stat_points int, PRIMARY KEY (entity_name))";
	sql << "CREATE TABLE items(entity_id int NOT NULL, entity_name varchar(20),agi int, str int, base_name varchar(50), curr_health int, max_health int, regen_amount int, item_type int, social_percentage int, enemy_type int, buy_price int, sell_price int, PRIMARY KEY (entity_id))";
	sql << "CREATE TABLE enemies(entity_id int NOT NULL, entity_name varchar(20), agi int, str int, base_name varchar(20), curr_health int, max_health int, regen_amount int, enemy_type int, social_skill int, PRIMARY KEY (entity_id))";
}

void dropTablesFromDB(session & sql)
{
	sql << "DROP TABLE bannedplayers";
	sql << "DROP TABLE inventory";
	sql << "DROP TABLE shortcuts";
	sql << "DROP TABLE players";
	sql << "DROP TABLE rooms";
	sql << "DROP TABLE items";
	sql << "DROP TABLE enemies";
}

void populateAllTables(session & sql)
{
	populateRooms(sql);
	populatePlayers(sql);
	populateItems(sql);
	populateEnemies(sql);
	populateShortcuts(sql);
}

void populatePlayers(session &sql)
{
	std::vector<Player> toInsert;

	Player p;

	p.setID(0);
	p.setEntName("lukas");
	p.setPassword("lukas");
	p.setRank(PlayerRank::Admin);
	p.setBsName("Lukas");
	p.setAGI(10);
	p.setSTR(10);
	p.setStatPoints(10);
	p.setCurrHealth(50);
	p.setMaxHealth(50);
	p.setRegenAmount(10);
	p.setCurrExp(0);
	p.setCurrLevel(1);
	p.setMaxLevel(100);
	p.setNextLevel(50);
	p.setMoney(10);
	p.setRoomID(1);
	p.setSpeed(5);
	Item i1;

	i1.setID(1);
	i1.setEntName("Sword");
	i1.m_baseStats.m_AGI = 100;
	i1.m_baseStats.m_STR = 20;
	i1.m_baseStats.m_BSTName = "Sword";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 0;
	i1.m_itemType = ItemType::Weapon;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 100;
	i1.m_price.m_SellPrice = 70;
	i1.m_socialBonus.m_enemyType = EnemyType::Troll;
	i1.m_socialBonus.m_socialPercentage = 100;
	p.getInventory().addItem(i1);
	i1.setID(2);
	i1.setEntName("Shield");
	i1.m_baseStats.m_AGI = 20;
	i1.m_baseStats.m_STR = 100;
	i1.m_baseStats.m_BSTName = "Shield";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 0;
	i1.m_itemType = ItemType::Armour;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 90;
	i1.m_price.m_SellPrice = 55;
	i1.m_socialBonus.m_enemyType = EnemyType::Dragon;
	i1.m_socialBonus.m_socialPercentage = 50;
	p.getInventory().addItem(i1);

	toInsert.emplace_back(p);

	p.setID(1);
	p.setEntName("paddy");
	p.setPassword("paddy");
	p.setRank(PlayerRank::Admin);

	//p.putInInventory(Item());

	toInsert.emplace_back(p);

	p.setID(2);
	p.setEntName("john");
	p.setPassword("john");
	p.setRank(PlayerRank::God);
	toInsert.emplace_back(p);

	p.setID(3);
	p.setEntName("paul");
	p.setPassword("paul");
	p.setRank(PlayerRank::Regular);
	toInsert.emplace_back(p);

	for (auto& player : toInsert)
	{
		sql << "insert into players(entity_id, entity_name, plr_password, str, agi, bs_name, curr_health, regen_amount, max_health, rank, curr_exp, curr_level,  max_level, next_level_xp, money, room_id, att_speed, stat_points) "
			"values(:entity_id, :entity_name, :plr_password, :str, :agi, :bs_name, :curr_health, :regen_amount, :max_health, :rank, :curr_exp, :curr_level, :max_level, :next_level_xp, :money, :room_id, :att_speed, :stat_points)", use(player);

		sql << "insert into inventory(entity_id, entity_name, pos0, pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9 ) "
			"values(:entity_id, :entity_name, :pos0, :pos1, :pos2, :pos3, :pos4, :pos5, :pos6, :pos7, :pos8, :pos9 )", use(player.getID(), "entity_id"), use(player.getEntName(), "entity_name"), use(player.getInventory());

	}
}

std::vector<Player> getPlayersFromDatabaseTable(session &sql)
{
	int count;
	sql << "select count(*) from players", into(count);

	std::vector<Player>myLoadedPlayers;
	myLoadedPlayers.reserve(count);
	Player player;

	for (int i = 1; i <= count; i++)
	{
		sql << "select * from items where entity_id = :entity_id", into(player), use(i);
		myLoadedPlayers.emplace_back(player);
	}

	return myLoadedPlayers;
}

void populateItems(session &sql)
{
	std::vector<Item> toInsert;

	Item i1;
	
	//Item i2(1, "Sword", 100, 20, "Sword", 100, 100, 0, ItemType::Weapon, true, 100, 70);

	i1.setID(1);
	i1.setEntName("Sword");
	i1.m_baseStats.m_AGI = 100;
	i1.m_baseStats.m_STR = 20;
	i1.m_baseStats.m_BSTName = "Sword";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 0;
	i1.m_itemType = ItemType::Weapon;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 100;
	i1.m_price.m_SellPrice = 70;
	i1.m_socialBonus.m_enemyType = EnemyType::Troll;
	i1.m_socialBonus.m_socialPercentage = 100;
	toInsert.emplace_back(i1);

	i1.setID(2);
	i1.setEntName("Shield");
	i1.m_baseStats.m_AGI = 20;
	i1.m_baseStats.m_STR = 100;
	i1.m_baseStats.m_BSTName = "Shield";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 0;
	i1.m_itemType = ItemType::Armour;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 90;
	i1.m_price.m_SellPrice = 55;
	i1.m_socialBonus.m_enemyType = EnemyType::Dragon;
	i1.m_socialBonus.m_socialPercentage = 50;
	toInsert.emplace_back(i1);

	i1.setID(3);
	i1.setEntName("Health Potion");
	i1.m_baseStats.m_AGI = 0;
	i1.m_baseStats.m_STR = 0;
	i1.m_baseStats.m_BSTName = "Health Potion";
	i1.m_baseStats.m_Health.m_currentHealth = 20;
	i1.m_baseStats.m_Health.m_maxHealth = 20;
	i1.m_baseStats.m_Health.m_regenAmount = 0;
	i1.m_itemType = ItemType::Potion;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 50;
	i1.m_price.m_SellPrice = 23;
	toInsert.emplace_back(i1);

	i1.setID(4);
	i1.setEntName("Key");
	i1.m_baseStats.m_AGI = 0;
	i1.m_baseStats.m_STR = 0;
	i1.m_baseStats.m_BSTName = "Key";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 100;
	i1.m_itemType = ItemType::Key;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 100;
	i1.m_price.m_SellPrice = 100;
	toInsert.emplace_back(i1);

	i1.setID(5);
	i1.setEntName("SwordR");
	i1.m_baseStats.m_AGI = 110;
	i1.m_baseStats.m_STR = 25;
	i1.m_baseStats.m_BSTName = "Regenerating Troll Killer";
	i1.m_baseStats.m_Health.m_currentHealth = 100;
	i1.m_baseStats.m_Health.m_maxHealth = 100;
	i1.m_baseStats.m_Health.m_regenAmount = 10;
	i1.m_itemType = ItemType::Weapon;
	i1.m_price.sellable = true;
	i1.m_price.m_buyPrice = 200;
	i1.m_price.m_SellPrice = 155;
	i1.m_socialBonus.m_enemyType = EnemyType::Witch;
	i1.m_socialBonus.m_socialPercentage = 70;
	toInsert.emplace_back(i1);

	for (const auto& item : toInsert)
	{
	sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type, buy_price, sell_price) "
		"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type, :buy_price, :sell_price)", use(item);
	}


	//Item item;
	////item.m_entity.m_ID = 1;
	//item.setID(1);
	////item.m_entity.m_name = "Sword";
	//item.setEntName("Sword");
	//sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
	//	"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	//item.setID(1);
	//item.setEntName("Shield");
	//sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
	//	"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	//item.setID(1);
	//item.setEntName("Arrow");
	//sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
	//	"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	///*item.m_entity.m_ID = 3;
	//item.m_entity.m_name = "Shield";
	//sql << "insert into items(entity_id, entity_name) "
	//	"values(:entity_id, :entity_name)", use(item);*/
}

std::vector<Item> getItemsFromDatabaseTable(session &sql, int howMany)
{
	std::vector<Item>myLoadedItems;
	myLoadedItems.reserve(howMany);
	Item item;
	for (int i = 1; i <= howMany; i++)
	{
		sql << "select * from items where entity_id = :entity_id", into(item), use(i);
		myLoadedItems.emplace_back(item);
	}

	/*int id = 123;
	sql << "delete from companies where id = " << id;*/

	return myLoadedItems;
	/*Item i1;
	sql << "select * from persons", into(p1);
	assert(p1.id == 1);
	assert(p1.firstName + p.lastName == "PatSmith");
	assert(p1.gender == "unknown");

	p.firstName = "Patricia";
	sql << "update persons set first_name = :first_name "
	"where id = :id", use(p);*/


}

std::vector<Item> getItemsFromDatabaseTable(session &sql)
{
	int count;
	sql << "select count(*) from items", into(count);

	std::vector<Item>myLoadedItems;
	myLoadedItems.reserve(count);
	Item item;

	for (int i = 1; i <= count; i++)
	{
		sql << "select * from items where entity_id = :entity_id", into(item), use(i);
		myLoadedItems.emplace_back(item);
	}

	return myLoadedItems;
}

void populateRooms(session & sql)
{
	std::vector<Room> toInsert;

	Room r1;
	r1.setID(1);
	r1.setEntName("Front Hall");
	r1.m_exits.m_east = 3;
	r1.m_exits.m_north = 12;
	r1.m_exits.m_south = 2;
	r1.m_exits.m_west = 4;
	r1.m_description = "An empty hall with no students in it. You can relax here. There is a giant banner that says \"Welcome to MONSTER SCHOOL!\" on it.";
	r1.m_roomType = RoomType::Common;
	toInsert.emplace_back(r1);

	r1.setID(2);
	r1.setEntName("Front Gate");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 1;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 0;
	r1.m_description = "A massive iron old style gate which is starting to rust. It is locked the key must be in the principal's office.";
	r1.m_roomType = RoomType::Event;
	toInsert.emplace_back(r1);

	r1.setID(3);
	r1.setEntName("Canteen");
	r1.m_exits.m_east = 7;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 1;
	r1.m_description = "Lots of students in here. Might get messy.";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(4);
	r1.setEntName("Locker Room");
	r1.m_exits.m_east = 1;
	r1.m_exits.m_north = 6;
	r1.m_exits.m_south = 5;
	r1.m_exits.m_west = 8;
	r1.m_description = "Lots of graffiti in here. It says \"RIP Masood\". I wonder what that means.";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(5);
	r1.setEntName("Girl's Toilet");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 4;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 0;
	r1.m_description = "There are girls who are angry at you for what you did last week. You really thought everyone would have forgotten by now.";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(6);
	r1.setEntName("Boy's Toilet");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 4;
	r1.m_exits.m_west = 0;
	r1.m_description = "There's an unsuspecting toilet. It would be a pity if someone put fireworks down the toilets again *wink wink*.";
	r1.m_roomType = RoomType::Event;
	toInsert.emplace_back(r1);

	r1.setID(7);
	r1.setEntName("Kitchen");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 3;
	r1.m_description = "There is a large cooker. It would be a shame if it caught fire *wink wink*. The Chef is not here.";
	r1.m_roomType = RoomType::Event;
	toInsert.emplace_back(r1);

	r1.setID(8);
	r1.setEntName("Gym");
	r1.m_exits.m_east = 4;
	r1.m_exits.m_north = 9;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 0;
	r1.m_description = "There people working out, they're all angry at you. You're not really liked here, are you?";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(9);
	r1.setEntName("Pitch");
	r1.m_exits.m_east = 10;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 8;
	r1.m_exits.m_west = 0;
	r1.m_description = "Some of your friends here. They're asking you for cigarettes. These are your only friends maybe you should help them *wink wink*.";
	r1.m_roomType = RoomType::Event;
	toInsert.emplace_back(r1);

	r1.setID(10);
	r1.setEntName("Backyard");
	r1.m_exits.m_east = 11;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 12;
	r1.m_exits.m_west = 9;
	r1.m_description = "A footpath with nice scenery. The only part of the school that isn't concrete. Still, no one likes you here.";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(11);
	r1.setEntName("Playground");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 10;
	r1.m_description = "A lot of kids selling junk. The closest thing to a black market you have. They get all their fireworks off the dark web.";
	r1.m_roomType = RoomType::Shop;
	toInsert.emplace_back(r1);

	r1.setID(12);
	r1.setEntName("Center Hall");
	r1.m_exits.m_east = 13;
	r1.m_exits.m_north = 10;
	r1.m_exits.m_south = 1;
	r1.m_exits.m_west = 15;
	r1.m_description = "You can see the school's emblem. It's an empty suit of armor with a sword pointing to its feet.";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);

	r1.setID(13);
	r1.setEntName("Library");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 14;
	r1.m_exits.m_west = 12;
	r1.m_description = "There are a lot of books and scrolls but none you're interested in. There's a shop keeper here with potions for health and other purposes.";
	r1.m_roomType = RoomType::Shop;
	toInsert.emplace_back(r1);

	r1.setID(14);
	r1.setEntName("Principles Office");
	r1.m_exits.m_east = 0;
	r1.m_exits.m_north = 13;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 0;
	r1.m_description = "There's a massive wooden table with the key to the front gate on it. Take it, now's your last chance.";
	r1.m_roomType = RoomType::Event;
	toInsert.emplace_back(r1);

	r1.setID(15);
	r1.setEntName("Maths Room");
	r1.m_exits.m_east = 12;
	r1.m_exits.m_north = 0;
	r1.m_exits.m_south = 0;
	r1.m_exits.m_west = 0;
	r1.m_description = "There are lots of tables and chairs. You hate this room, why did you even come here?";
	r1.m_roomType = RoomType::Standard;
	toInsert.emplace_back(r1);


	for (const auto& room : toInsert)
	{
		sql << "insert into rooms(entity_id, entity_name, east, north, south, west, description, room_type) "
			"values(:entity_id, :entity_name, :east, :north, :south, :west, :description, :room_type)", use(room);
	}

}

std::vector<Room> getRoomsFromDatabaseTable(session & sql)
{
	//neet to know how many is there to populate
	int count;
	sql << "select count(*) from rooms", into(count);

	std::vector<Room>myLoadedRooms;
	myLoadedRooms.reserve(count);
	Room room;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from rooms where entity_id = :entity_id", into(room), use(i);
		myLoadedRooms.emplace_back(room);
	}

	return myLoadedRooms;
}

void populateEnemies(session& sql)
{
	std::vector<Enemy> toInsert;

	Enemy e1;

	e1.setID(1);
	e1.setEntName("Troll");
	e1.m_enemyStats.m_AGI = 10;
	e1.m_enemyStats.m_STR = 10;
	e1.m_enemyStats.m_BSTName = "Tony";
	e1.m_enemyStats.m_Health.m_currentHealth = 50;
	e1.m_enemyStats.m_Health.m_maxHealth = 50;
	e1.m_enemyStats.m_Health.m_regenAmount = 10;
	e1.m_type = EnemyType::Troll;
	e1.m_socialSkill = 1;
	toInsert.emplace_back(e1);

	e1.setID(2);
	e1.setEntName("Dragon");
	e1.m_enemyStats.m_AGI = 10;
	e1.m_enemyStats.m_STR = 10;
	e1.m_enemyStats.m_BSTName = "Dom";
	e1.m_enemyStats.m_Health.m_currentHealth = 50;
	e1.m_enemyStats.m_Health.m_maxHealth = 75;
	e1.m_enemyStats.m_Health.m_regenAmount = 12;
	e1.m_type = EnemyType::Dragon;
	e1.m_socialSkill = 5;
	toInsert.emplace_back(e1);

	e1.setID(3);
	e1.setEntName("Witch");
	e1.m_enemyStats.m_AGI = 9;
	e1.m_enemyStats.m_STR = 9;
	e1.m_enemyStats.m_BSTName = "Mary";
	e1.m_enemyStats.m_Health.m_currentHealth = 40;
	e1.m_enemyStats.m_Health.m_maxHealth = 100;
	e1.m_enemyStats.m_Health.m_regenAmount = 15;
	e1.m_type = EnemyType::Witch;
	e1.m_socialSkill = 5;
	toInsert.emplace_back(e1);

	e1.setID(4);
	e1.setEntName("Skeleton");
	e1.m_enemyStats.m_AGI = 10;
	e1.m_enemyStats.m_STR = 8;
	e1.m_enemyStats.m_BSTName = "Cranky";
	e1.m_enemyStats.m_Health.m_currentHealth = 50;
	e1.m_enemyStats.m_Health.m_maxHealth = 50;
	e1.m_enemyStats.m_Health.m_regenAmount = 20;
	e1.m_type = EnemyType::Skeleton;
	e1.m_socialSkill = 5;
	toInsert.emplace_back(e1);

	e1.setID(5);
	e1.setEntName("Ghost");
	e1.m_enemyStats.m_AGI = 10;
	e1.m_enemyStats.m_STR = 10;
	e1.m_enemyStats.m_BSTName = "Shiny";
	e1.m_enemyStats.m_Health.m_currentHealth = 6;
	e1.m_enemyStats.m_Health.m_maxHealth = 50;
	e1.m_enemyStats.m_Health.m_regenAmount = 2;
	e1.m_type = EnemyType::Ghost;
	e1.m_socialSkill = 5;
	toInsert.emplace_back(e1);

	for (const auto& enemy : toInsert)
	{
		sql << "insert into enemies(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, enemy_type, social_skill) "
			"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :enemy_type, :social_skill)", use(enemy);
	}
}

std::vector<Enemy> getEnemiesFromDatabaseTable(session & sql)
{
	//neet to know how many is there to populate
	int count;
	sql << "select count(*) from enemies", into(count);

	std::vector<Enemy>myLoadedEnemies;
	myLoadedEnemies.reserve(count);
	Enemy enemy;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from enemies where entity_id = :entity_id", into(enemy), use(i);
		myLoadedEnemies.emplace_back(enemy);
	}

	return myLoadedEnemies;
}

void populateShortcuts(session & sql)
{
	std::vector<Shortcuts> toInsert;

	Shortcuts plrShortcuts;
	plrShortcuts.setEntName("lukas");
	plrShortcuts.setID(0);
	plrShortcuts.m_shortcuts.emplace_back("chang");
	plrShortcuts.m_shortcuts.emplace_back("x");
	plrShortcuts.m_shortcuts.emplace_back("ch");
	plrShortcuts.m_shortcuts.emplace_back("sh");
	plrShortcuts.m_shortcuts.emplace_back("n");
	plrShortcuts.m_shortcuts.emplace_back("s");
	plrShortcuts.m_shortcuts.emplace_back("e");
	plrShortcuts.m_shortcuts.emplace_back("w");
	plrShortcuts.m_shortcuts.emplace_back("wh");
	plrShortcuts.m_shortcuts.emplace_back("a");
	plrShortcuts.m_shortcuts.emplace_back("p");
	plrShortcuts.m_shortcuts.emplace_back("u");
	plrShortcuts.m_shortcuts.emplace_back("h");
	plrShortcuts.m_shortcuts.emplace_back("who");
	plrShortcuts.m_shortcuts.emplace_back("whll");
	plrShortcuts.m_shortcuts.emplace_back("prm");
	plrShortcuts.m_shortcuts.emplace_back("de");
	plrShortcuts.m_shortcuts.emplace_back("l");
	plrShortcuts.m_shortcuts.emplace_back("g");
	plrShortcuts.m_shortcuts.emplace_back("dd");
	toInsert.emplace_back(plrShortcuts);

	plrShortcuts.setID(1);
	plrShortcuts.setEntName("paddy");
	toInsert.emplace_back(plrShortcuts);

	plrShortcuts.setID(2);
	plrShortcuts.setEntName("john");
	toInsert.emplace_back(plrShortcuts);

	plrShortcuts.setID(3);
	plrShortcuts.setEntName("paul");
	toInsert.emplace_back(plrShortcuts);

	for (const auto& shc : toInsert)
	{
		sql << "insert into shortcuts(entity_id, entity_name, change, stat, chat, shut, north, south, east, west, whisper, attack, pick, use, help, who, whoall, promote, demote, look, get, distract ) "
		"values(:entity_id, :entity_name, :change, :stat, :chat, :shut, :north, :south, :east, :west, :whisper, :attack, :pick, :use, :help, :who, :whoall, :promote, :demote, :look, :get, :distract ) ", use(shc);
	}
}

void speedtestInserts(session & sql)
{
	transaction tr(sql);

	sql << "DROP TABLE customers";
	sql << "CREATE TABLE customers(id int NOT NULL, name varchar(20), pass varchar(64), PRIMARY KEY (id))";
	int i{ 0 };
	std::string name{ "lukas" };
	std::string pass{ "pass" };

	statement st = (sql.prepare <<"insert into customers(id, name, pass) "
			"values(:id, :name, :pass)", use(i), use(name), use(pass));
	//this is the timing part of the code
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	
	

	
	for (i = 0; i < 10000; i++)
	{
		if (i % 2 == 0)
		{
			name = "even";
		}
		else
		{
			name = "odd";
		}
		
		st.execute(true);
		/*sql << "insert into customers(id, name, pass) "
			"values(:id, :name, :pass)", use(i), use(name), use(pass);*/
	}
	
	tr.commit();
	end = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end - start;
	cout << "Request took: " << elapsed_seconds.count() << " seconds" << endl;


	//transaction tr(sql);

	//sql << "DROP TABLE numbers";
	//sql << "CREATE TABLE numbers(id int)";

	////this is the timing part of the code
	//std::chrono::time_point<std::chrono::system_clock> start, end;

	//// Example 3.
	//const int BATCH_SIZE = 5000;
	//std::vector<int> valsIn;
	//valsIn.reserve(BATCH_SIZE);
	//for (int i = 0; i != BATCH_SIZE; ++i)
	//{
	//	valsIn.emplace_back(i);
	//}
	//statement st = (sql.prepare <<
	//	"insert into numbers(id) values(:id)",
	//	use(valsIn));



	//start = std::chrono::system_clock::now();

	////st.execute(true);

	//for (int i = 0; i != 4; ++i)
	//{
	//	st.execute(true);
	//}

	//tr.commit();


	//end = std::chrono::system_clock::now();

	//std::chrono::duration<double> elapsed_seconds = end - start;
	//cout << "Request took: " << elapsed_seconds.count() << " seconds" << endl;
}
