#include "Room.h"

void Room::printRoom() const
{
	m_entity.printEntity();
}

Room::Room(Entity entity, RoomExits exits, std::string description, std::vector<Enemy> enemies, std::vector<Item> items, RoomType roomType)
	:m_entity(entity), m_exits(exits), m_description(description), m_enemies(enemies), m_items(items), m_roomType(roomType)
{

}
